package com.kkrasylnykov.l13_customlayoutexample.customViews;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kkrasylnykov.l13_customlayoutexample.R;

public class ImageAndTextView extends RelativeLayout {
    private TextView m_tv;
    private ImageView m_iv;

    public ImageAndTextView(Context context) {
        super(context);
        init(context);
    }

    public ImageAndTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ImageAndTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.customview_image_and_text_view,this,true);
        m_tv = (TextView) findViewById(R.id.text);
        m_iv = (ImageView) findViewById(R.id.image);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int nWidthText = MeasureSpec.getSize(widthMeasureSpec) - m_iv.getMeasuredWidth() - 20; //20 - margin
        int widthTextMeasureSpec = MeasureSpec.makeMeasureSpec(nWidthText, MeasureSpec.EXACTLY);
        int nHeightMax = m_iv.getMeasuredHeight();
        LayoutParams params = (LayoutParams) m_tv.getLayoutParams();
        params.setMargins(m_iv.getMeasuredWidth() + 20,0,0,0);
        m_tv.measure(widthTextMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
        int nRealTextHeight = m_tv.getMeasuredHeight();
        if (nHeightMax<nRealTextHeight) {
            float fDelta = 1;//((float) nHeightMax) / nRealTextHeight;
            float fTextSize = m_tv.getTextSize() * fDelta;
            m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
            m_tv.measure(widthTextMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            nRealTextHeight = m_tv.getMeasuredHeight();
            while (nHeightMax < nRealTextHeight) {
                fTextSize -= 5;
                m_tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
                m_tv.measure(widthTextMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                nRealTextHeight = m_tv.getMeasuredHeight();
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
