package com.kkrasylnykov.l14_savedataexample.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.AppSettings;

public class TermsActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        findViewById(R.id.yesTermsActivity).setOnClickListener(this);
        findViewById(R.id.noTermsActivity).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.yesTermsActivity:
                setResult(RESULT_OK);
                AppSettings settings = new AppSettings(this);
                settings.setIsTermsAccept(true);
                break;
        }
        finish();
    }
}
