package com.kkrasylnykov.l14_savedataexample.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.DB.DBHelper;
import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.DBConstants;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_TERMS = 101;

    private LinearLayout m_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);
        if (!settings.isTermsAccept()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_TERMS);
        }

        findViewById(R.id.btnAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveAllUserMainActivity).setOnClickListener(this);
        m_container = (LinearLayout) findViewById(R.id.containerMAinActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateScreen();
    }

    private void updateScreen(){
        m_container.removeAllViews();
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(DBConstants.TABLE_NAME_USER_INFO, null, null, null, null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do {
                    long nId = cursor.getLong(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_ID));
                    String strName = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_NAME));
                    String strSName = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_SNAME));
                    String strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_PHONE));
                    String strMail = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_MAIL));

                    Log.d("devcpp", "nId -> " + nId + "; strName -> " + strName +
                            "; strSName -> " + strSName + "; strPhone -> " + strPhone +
                            "; strMail -> " + strMail);
                    TextView text = new TextView(this);
                    text.setText(strName + " " + strSName + "\nPhone: " + strPhone);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(0,5,0,5);
                    text.setLayoutParams(layoutParams);
                    text.setOnClickListener(this);
                    text.setTag(nId);
                    m_container.addView(text);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getTag()!=null){
            long nId = (long) view.getTag();
            openAddActivity(nId);
        } else {
            switch (view.getId()){
                case R.id.btnAddUserMainActivity:
                    openAddActivity();
                    break;
                case R.id.btnRemoveAllUserMainActivity:
                    removeAllUsers();
                    break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_TERMS && resultCode!=RESULT_OK){
            finish();
        }
    }

    private void removeAllUsers(){
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        db.delete(DBConstants.TABLE_NAME_USER_INFO,null,null);

        db.close();

        updateScreen();
    }

    private void openAddActivity(long nId){
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra(EditUserActivity.KEY_USER_ID, nId);
        startActivity(intent);
    }

    private void openAddActivity(){
        Intent intent = new Intent(this, EditUserActivity.class);
        startActivity(intent);
    }
}
