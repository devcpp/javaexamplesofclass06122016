package com.kkrasylnykov.l14_savedataexample.Activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kkrasylnykov.l14_savedataexample.DB.DBHelper;
import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.DBConstants;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String KEY_USER_ID = "KEY_USER_ID";


    private EditText m_nameEditText;
    private EditText m_snameEditText;
    private EditText m_phoneEditText;
    private EditText m_mailEditText;

    private long m_nUserId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nUserId = bundle.getLong(KEY_USER_ID, -1);
            }
        }

        m_nameEditText = (EditText) findViewById(R.id.editTextNameEditUserActivity);
        m_snameEditText = (EditText) findViewById(R.id.editTextSNameEditUserActivity);
        m_phoneEditText = (EditText) findViewById(R.id.editTextPhoneEditUserActivity);
        m_mailEditText = (EditText) findViewById(R.id.editTextMailEditUserActivity);

        Button btnAdd = (Button) findViewById(R.id.btnAddUserEditUserActivity);
        btnAdd.setOnClickListener(this);

        if (m_nUserId>=0){
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.btnRemoveUserEditUserActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            DBHelper dbHelper = new DBHelper(this);
            SQLiteDatabase db = dbHelper.getReadableDatabase();

            String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
            String[] arrArgs = new String[]{Long.toString(m_nUserId)};

            Cursor cursor = db.query(DBConstants.TABLE_NAME_USER_INFO,null,
                    strSelect, arrArgs,null, null, null);
            if (cursor!=null){
                if (cursor.moveToFirst()){
                    m_nameEditText.setText(cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_NAME)));
                    m_snameEditText.setText(cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_SNAME)));
                    m_phoneEditText.setText(cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_PHONE)));
                    m_mailEditText.setText(cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_MAIL)));
                }
                cursor.close();
            }
            db.close();
        }

    }

    @Override
    public void onClick(View view) {
        String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(m_nUserId)};
        switch (view.getId()){
            case R.id.btnAddUserEditUserActivity:
                String strName = m_nameEditText.getText().toString();
                String strSName = m_snameEditText.getText().toString();
                String strPhone = m_phoneEditText.getText().toString();
                String strMail = m_mailEditText.getText().toString();
                if (validate(strName, strSName, strPhone, strMail)){
                    DBHelper dbHelper = new DBHelper(this);
                    SQLiteDatabase db = dbHelper.getWritableDatabase();

                    ContentValues values = new ContentValues();
                    values.put(DBConstants.USER_INFO_FIELD_NAME_NAME, strName);
                    values.put(DBConstants.USER_INFO_FIELD_NAME_SNAME, strSName);
                    values.put(DBConstants.USER_INFO_FIELD_NAME_PHONE, strPhone);
                    values.put(DBConstants.USER_INFO_FIELD_NAME_MAIL, strMail);

                    if(m_nUserId>=0){
                        db.update(DBConstants.TABLE_NAME_USER_INFO, values, strSelect, arrArgs);
                    } else {
                        db.insert(DBConstants.TABLE_NAME_USER_INFO, null, values);
                    }

                    db.close();

                    if(m_nUserId>=0) {
                        finish();
                    } else {
                        AppSettings settings = new AppSettings(this);
                        if (settings.isCloseActivity()){
                            finish();
                        } else {
                            m_nameEditText.setText("");
                            m_nameEditText.requestFocus();
                            m_snameEditText.setText("");
                            m_phoneEditText.setText("");
                            m_mailEditText.setText("");
                        }
                    }


                }
                break;
            case R.id.btnRemoveUserEditUserActivity:
                DBHelper dbHelper = new DBHelper(this);
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.delete(DBConstants.TABLE_NAME_USER_INFO, strSelect, arrArgs);
                db.close();
                finish();
                break;
        }
    }

    private boolean validate(String strName, String strSName, String strPhone, String strMail){
        return strName != null && !strName.isEmpty();
    }
}
