package com.kkrasylnykov.l14_savedataexample.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kkrasylnykov.l14_savedataexample.ToolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + DBConstants.TABLE_NAME_USER_INFO +
                " (" + DBConstants.USER_INFO_FIELD_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBConstants.USER_INFO_FIELD_NAME_NAME + " TEXT NOT NULL, "
                + DBConstants.USER_INFO_FIELD_NAME_SNAME + " TEXT, "
                + DBConstants.USER_INFO_FIELD_NAME_PHONE + " TEXT, "
                + DBConstants.USER_INFO_FIELD_NAME_MAIL + " TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
