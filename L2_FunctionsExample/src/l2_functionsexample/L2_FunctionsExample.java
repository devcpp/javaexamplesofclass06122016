package l2_functionsexample;

import java.util.Scanner;

public class L2_FunctionsExample {
    
    public static int pow(int nVal){
        int nResult = nVal*nVal;
        return nResult;
    }
    
    public static int pow(int nVal, int nPow){
        int nResult = 0;
        if (nPow==0){
            nResult = 1;
        } else {
           nResult =  nVal * pow(nVal,nPow-1);
        }
        return nResult;
    }
    
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        
        int nA = scan.nextInt();
        int nResultA = pow(nA,6);
        
        int nB = scan.nextInt();
        int nResultB = pow(nB,3);
        
        int nC = scan.nextInt();
        int nResultC = pow(nC,2);
        
        System.out.println("nResultA >> " + nResultA);
        System.out.println("nResultB >> " + nResultB);
        System.out.println("nResultC >> " + nResultC);
    }
    
}
