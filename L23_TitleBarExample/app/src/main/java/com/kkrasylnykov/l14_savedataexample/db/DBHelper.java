package com.kkrasylnykov.l14_savedataexample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION);
        Log.d("devcpp","DBHelper -> ");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("devcpp","DBHelper -> onCreate -> ");
        sqLiteDatabase.execSQL("CREATE TABLE " + DBConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME +
                " (" + DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME + " TEXT NOT NULL, "
                + DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME + " TEXT, "
                + DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL + " TEXT);");

        sqLiteDatabase.execSQL("CREATE TABLE " + DBConstants.DB_V2.TABLE_PHONES.TABLE_NAME +
                " (" + DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID + " INTEGER NOT NULL, "
                + DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_PHONE + " TEXT NOT NULL);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.d("devcpp","DBHelper -> onUpgrade -> " + i);
        Log.d("devcpp","DBHelper -> onUpgrade -> " + i1);

        sqLiteDatabase.execSQL("CREATE TABLE " + DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME + "_TMP" +
                " (" + DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME + " TEXT NOT NULL, "
                + DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME + " TEXT, "
                + DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_PHONE + " TEXT, "
                + DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL + " TEXT);");

        Cursor cursor = sqLiteDatabase.query(DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME, null,null,null, null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    ContentValues values = new ContentValues();
                    values.put(DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME, cursor.getString(1));
                    values.put(DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME, cursor.getString(2));
                    values.put(DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_PHONE, cursor.getString(3));
                    values.put(DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL, cursor.getString(4));
                    sqLiteDatabase.insert(DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME + "_TMP", null, values);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }

        sqLiteDatabase.execSQL("DROP TABLE " + DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME);

        onCreate(sqLiteDatabase);

        cursor = sqLiteDatabase.query(DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME + "_TMP", null,null,null, null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do{
                    ContentValues values = new ContentValues();
                    values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME, cursor.getString(1));
                    values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME, cursor.getString(2));
                    values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL, cursor.getString(4));
                    long nUserId = sqLiteDatabase.insert(DBConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME, null, values);

                    values = new ContentValues();
                    values.put(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_PHONE, cursor.getString(3));
                    values.put(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID, nUserId);
                    sqLiteDatabase.insert(DBConstants.DB_V2.TABLE_PHONES.TABLE_NAME, null, values);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        sqLiteDatabase.execSQL("DROP TABLE " + DBConstants.DB_V1.TABLE_USER_INFO.TABLE_NAME + "_TMP");
    }
}
