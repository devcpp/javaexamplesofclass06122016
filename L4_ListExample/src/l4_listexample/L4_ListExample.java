package l4_listexample;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class L4_ListExample {
    
    public static void outArray(List<Float> list){
        for (Float fVal:list){
            System.out.print(" " + fVal);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        LinkedList<Float> arrData = new LinkedList<>();
        do{
            Float fSize = scanner.nextFloat();
            if (fSize>0){
                arrData.add(fSize);
            } else {
                break;//continue;
            }
        }while(true);
        
        //outArray(arrData);
        
        //arrData.set(2, 10.6f);
        //arrData.remove(2);
        //boolean bRes = arrData.remove(2.0f);
        
        //outArray(arrData);
        
        //System.out.println("bRes -> " + bRes);
        //System.out.println("arrData.size -> " + arrData.size());
        //System.out.println("arrData.get -> " + arrData.get(0));
        //System.out.println("arrData.get -> " + arrData.get(arrData.size()-1));
        //System.out.println("arrData.indexOf -> " + arrData.indexOf(new Float(3)));
        //System.out.println("arrData.contains -> " + arrData.contains(4.0f));
        
        float fSum = 0;
        for (Float fVal:arrData){
            fSum+=fVal;
        }
        
        System.out.println("fSum = " + fSum);
    }
    
}
