package com.kkrasylnykov.l14_savedataexample.controller.wrappers.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class UserInfoDBWrapper extends BaseDBWrapper {

    public UserInfoDBWrapper(Context context) {
        super(context, DBConstants.TABLE_NAME_USER_INFO);
    }

    public ArrayList<UserInfo> getAll(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null) {
            if (cursor.moveToFirst()) {
                do {
                    UserInfo userInfo = new UserInfo(cursor);
                    arrResult.add(userInfo);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public UserInfo getUserById(long nId){
        UserInfo userInfo = null;
        SQLiteDatabase db = getReadableDB();

        String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nId)};

        Cursor cursor = db.query(getTableName(),null,
                strSelect, arrArgs,null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                userInfo = new UserInfo(cursor);
            }
            cursor.close();
        }
        db.close();
        return userInfo;
    }

    public ArrayList<UserInfo> getUserByField(String strSearch){
        String strSearchProcessing = strSearch + "%";
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        String strSelect = DBConstants.USER_INFO_FIELD_NAME_NAME + " LIKE ? OR " +
                DBConstants.USER_INFO_FIELD_NAME_SNAME + " LIKE ? OR " +
                DBConstants.USER_INFO_FIELD_NAME_PHONE + " LIKE ? OR " +
                DBConstants.USER_INFO_FIELD_NAME_MAIL + " LIKE ?";
        String[] arrArgs = new String[]{strSearchProcessing , strSearchProcessing, strSearchProcessing, strSearchProcessing};

        Cursor cursor = db.query(getTableName(),null,
                strSelect, arrArgs,null, null, null);
        if (cursor!=null) {
            if (cursor.moveToFirst()) {
                do {
                    UserInfo userInfo = new UserInfo(cursor);
                    arrResult.add(userInfo);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public void update(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        ContentValues values = item.getContentValues();
        db.update(getTableName(), values, strSelect, arrArgs);
        db.close();

    }

    public void insert(UserInfo item){
        SQLiteDatabase db = getWritableDB();

        ContentValues values = item.getContentValues();
        db.insert(getTableName(), null, values);
        db.close();

    }

    public void remove(UserInfo item){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        db.delete(getTableName(), strSelect, arrArgs);
        db.close();
    }

    public void removeById(long nId){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strSelect, arrArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();

        db.delete(getTableName(),null,null);

        db.close();
    }
}
