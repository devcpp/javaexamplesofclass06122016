package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String KEY_USER_ID = "KEY_USER_ID";


    private EditText m_nameEditText;
    private EditText m_snameEditText;
    private EditText m_phoneEditText;
    private EditText m_mailEditText;

    private long m_nUserId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nUserId = bundle.getLong(KEY_USER_ID, -1);
            }
        }

        m_nameEditText = (EditText) findViewById(R.id.editTextNameEditUserActivity);
        m_snameEditText = (EditText) findViewById(R.id.editTextSNameEditUserActivity);
        m_phoneEditText = (EditText) findViewById(R.id.editTextPhoneEditUserActivity);
        m_mailEditText = (EditText) findViewById(R.id.editTextMailEditUserActivity);

        Button btnAdd = (Button) findViewById(R.id.btnAddUserEditUserActivity);
        btnAdd.setOnClickListener(this);

        if (m_nUserId>=0){
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.btnRemoveUserEditUserActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = userInfoEngine.getUserById(m_nUserId);
            m_nameEditText.setText(userInfo.getName());
            m_snameEditText.setText(userInfo.getSName());
            m_phoneEditText.setText(userInfo.getPhone());
            m_mailEditText.setText(userInfo.getMail());
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAddUserEditUserActivity:
                UserInfo userInfo = new UserInfo(m_nameEditText.getText().toString(),
                        m_snameEditText.getText().toString(),
                        m_phoneEditText.getText().toString(),
                        m_mailEditText.getText().toString());
                if (userInfo.validate()){
                    UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                    if(m_nUserId>=0){
                        userInfo.setId(m_nUserId);
                        userInfoEngine.update(userInfo);
                    } else {
                        userInfoEngine.insert(userInfo);
                    }

                    if(m_nUserId>=0) {
                        finish();
                    } else {
                        AppSettings settings = new AppSettings(this);
                        if (settings.isCloseActivity()){
                            finish();
                        } else {
                            m_nameEditText.setText("");
                            m_nameEditText.requestFocus();
                            m_snameEditText.setText("");
                            m_phoneEditText.setText("");
                            m_mailEditText.setText("");
                        }
                    }
                }
                break;
            case R.id.btnRemoveUserEditUserActivity:
                UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                userInfoEngine.removeById(m_nUserId);
                finish();
                break;
        }
    }
}
