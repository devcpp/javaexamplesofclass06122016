package com.kkrasylnykov.l14_savedataexample.controller.engines;

import android.content.Context;

import com.kkrasylnykov.l14_savedataexample.controller.wrappers.db.UserInfoDBWrapper;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {

    public UserInfoEngine(Context context) {
        super(context);
    }

    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        return dbWrapper.getAll();
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        return dbWrapper.getUserById(nId);
    }

    public ArrayList<UserInfo> getUserByField(String strSearch){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        return dbWrapper.getUserByField(strSearch);
    }

    public void update(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.update(item);
    }

    public void insert(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.insert(item);
    }

    public void remove(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.remove(item);
    }

    public void removeById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.removeById(nId);
    }

    public void removeAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.removeAll();
    }
}
