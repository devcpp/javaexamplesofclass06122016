package com.kkrasylnykov.l14_savedataexample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import java.util.List;

public class SampleAdapter extends BaseAdapter implements Filterable {

    private List<UserInfo> usersList;
    private UserInfoEngine engine;
    private LayoutInflater inflater;

    public SampleAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        engine = new UserInfoEngine(context);
        usersList = engine.getAll();
    }

    @Override
    public int getCount() {
        return usersList.size();
    }

    @Override
    public UserInfo getItem(int i) {
        return usersList.isEmpty() ? null : usersList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final UserInfo userInfo = getItem(i);
        ViewHolder holder;
        View itemView;
        if (view == null) {
            holder = new ViewHolder();
            itemView = inflater.inflate(R.layout.item_user_info, viewGroup, false);
            holder.nameTextView = (TextView) itemView.findViewById(R.id.textViewName);
            holder.phoneTextView = (TextView) itemView.findViewById(R.id.textViewPhone);
            holder.nameBtn = (Button) itemView.findViewById(R.id.btnName);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
            itemView = view;
        }
        holder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
        //holder.phoneTextView.setText("Phone: " + userInfo.getPhone());
        holder.nameBtn.findViewById(R.id.btnName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), userInfo.getName(), Toast.LENGTH_SHORT).show();
            }
        });
        return itemView;
    }

    public void updateList() {
        usersList = engine.getAll();
        super.notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                List<UserInfo> userInfoList = engine.getUserByField(charSequence.toString());
                FilterResults filterResults = new FilterResults();
                filterResults.count = userInfoList.size();
                filterResults.values = userInfoList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                usersList = (List<UserInfo>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private class ViewHolder {
        TextView nameTextView;
        TextView phoneTextView;
        Button nameBtn;
    }

    //    private void updateScreen(){
//        m_container.removeAllViews();
//        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
//        ArrayList<UserInfo> arrData;
//        if (m_strSearch.isEmpty()){
//            arrData = userInfoEngine.getAll();
//        } else {
//            arrData =
//        }
//
//        for (UserInfo userInfo : arrData){

//            m_container.addView(text);
//        }
//    }
}
