package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.adapters.RecyclerAdapter;
import com.kkrasylnykov.l14_savedataexample.adapters.SampleAdapter;
import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_TERMS = 101;
    public static final int MOCK_USERS_COUNT = 30;
    public static final String NAME_SUFFIX = "_Name";
    public static final String SECOND_NAME_SUFFIX = "_SName";
    public static final String PHONE_PREFIX = "095444444";
    public static final String EMAIL_SUFFIX = "@test.com";

    private String m_strSearch = "";
    private RecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);
        if (!settings.isTermsAccept()) {
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_TERMS);
        }

        EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //TODO adapter.getFilter().filter(editable);
            }
        });

        findViewById(R.id.btnAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveAllUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnAddMockUsersMainActivity).setOnClickListener(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        adapter = new RecyclerAdapter(this);
        adapter.setOnClickItem(new RecyclerAdapter.OnItemClickRecyclerAdapterListener() {
            @Override
            public void onItemClick(UserInfo item) {
                openAddActivity(item.getId());
            }
        });
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.updateList();
    }


    /*private void addMockUsers() {
        UserInfoEngine engine = new UserInfoEngine(this);
        for (int i = 0; i < MOCK_USERS_COUNT; i++) {
            String name = i + NAME_SUFFIX;
            String sName = i + SECOND_NAME_SUFFIX;
            String phone = PHONE_PREFIX + i;
            String email = name + EMAIL_SUFFIX;
            UserInfo userInfo = new UserInfo(name, sName, phone, email);
            engine.insert(userInfo);
        }
        adapter.updateList();
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddUserMainActivity:
                openAddActivity();
                break;
            case R.id.btnRemoveAllUserMainActivity:
                //removeAllUsers();
                adapter.removeItemTest();
                break;

            case R.id.btnAddMockUsersMainActivity:
                //addMockUsers();
                adapter.addItemTest();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TERMS && resultCode != RESULT_OK) {
            finish();
        }
    }

    private void removeAllUsers() {
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        userInfoEngine.removeAll();
        adapter.updateList();
    }

    private void openAddActivity(long nId) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra(EditUserActivity.KEY_USER_ID, nId);
        startActivity(intent);
    }

    private void openAddActivity() {
        Intent intent = new Intent(this, EditUserActivity.class);
        startActivity(intent);
    }
}
