package com.kkrasylnykov.l14_savedataexample.controller.wrappers.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;

public abstract class BaseDBWrapper {

    private DBHelper m_DBHelper;
    private String m_strTableName;

    public BaseDBWrapper(Context context, String strTableName){
        m_DBHelper = new DBHelper(context);
        m_strTableName = strTableName;
    }

    protected String getTableName(){
        return m_strTableName;
    }

    protected SQLiteDatabase getReadableDB(){
        return m_DBHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritableDB(){
        return m_DBHelper.getWritableDatabase();
    }

}
