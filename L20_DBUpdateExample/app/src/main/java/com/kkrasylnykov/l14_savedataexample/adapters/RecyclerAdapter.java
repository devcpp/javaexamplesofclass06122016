package com.kkrasylnykov.l14_savedataexample.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1001;
    private static final int TYPE_FIRST_TITLE = 1002;
    private static final int TYPE_SECOND_TITLE = 1003;

    private List<UserInfo> usersList;
    private UserInfoEngine engine;
    private LayoutInflater inflater;

    private OnItemClickRecyclerAdapterListener listener;

    public RecyclerAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        engine = new UserInfoEngine(context);
        usersList = engine.getAll();
    }

    @Override
    public int getItemCount() {
        return usersList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        int nResult = TYPE_ITEM;
        if (position==0) {
            nResult = TYPE_FIRST_TITLE;
        } else if (position == getItemCount()-1) {
            nResult = TYPE_SECOND_TITLE;
        }
        return nResult;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolderResult = null;
        switch (viewType){
            case TYPE_ITEM:
                View itemView = inflater.inflate(R.layout.item_user_info, parent, false);
                viewHolderResult = new ItemViewHolder(itemView);
                break;
            case TYPE_FIRST_TITLE:
                View viewFirst = inflater.inflate(R.layout.item_first, parent, false);
                viewHolderResult = new FirstTitleViewHolder(viewFirst);
                break;
            case TYPE_SECOND_TITLE:
                View viewLast = inflater.inflate(R.layout.item_last, parent, false);
                viewHolderResult = new LastTitleViewHolder(viewLast);
                break;
        }
        return viewHolderResult;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case TYPE_ITEM:
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                final UserInfo userInfo = usersList.get(position-1);
                itemViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener!=null){
                            listener.onItemClick(userInfo);
                        }
                    }
                });
                itemViewHolder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
                String strPhones = "";
                for (PhoneInfo phone: userInfo.getPhones()){
                    strPhones += phone.getPhone() + "/n";
                }
                itemViewHolder.phoneTextView.setText("Phones: " + strPhones);
                itemViewHolder.nameBtn.findViewById(R.id.btnName).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(), userInfo.getName(), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
        }
    }

    public void updateList() {
        usersList = engine.getAll();
        super.notifyDataSetChanged();
    }

    public void removeItemTest() {
        usersList = engine.getAll();
        usersList.remove(1);
        super.notifyItemRemoved(2);
    }

    public void addItemTest() {
        usersList = engine.getAll();
        super.notifyItemInserted(3);
    }

    public void setOnClickItem(OnItemClickRecyclerAdapterListener listener){
        this.listener = listener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        View rootView;
        TextView nameTextView;
        TextView phoneTextView;
        Button nameBtn;

        public ItemViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.textViewName);
            phoneTextView = (TextView) itemView.findViewById(R.id.textViewPhone);
            nameBtn = (Button) itemView.findViewById(R.id.btnName);
        }
    }

    public class FirstTitleViewHolder extends RecyclerView.ViewHolder{
        View rootView;

        public FirstTitleViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
        }
    }

    public class LastTitleViewHolder extends RecyclerView.ViewHolder{
        View rootView;

        public LastTitleViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
        }
    }

    public interface OnItemClickRecyclerAdapterListener {
        void onItemClick(UserInfo item);
    }
}
