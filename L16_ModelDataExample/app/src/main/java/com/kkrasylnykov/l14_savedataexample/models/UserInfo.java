package com.kkrasylnykov.l14_savedataexample.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class UserInfo {
    private long m_nId = -1;
    private String m_strName;
    private String m_strSName;
    private String m_strPhone;
    private String m_strMail;

    public UserInfo(String m_strName, String m_strSName, String m_strPhone, String m_strMail) {
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_strPhone = m_strPhone;
        this.m_strMail = m_strMail;
    }

    public UserInfo(Cursor cursor){
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_ID));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_NAME));
        this.m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_SNAME));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_PHONE));
        this.m_strMail = cursor.getString(cursor.getColumnIndex(DBConstants.USER_INFO_FIELD_NAME_MAIL));
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public String getMail() {
        return m_strMail;
    }

    public void setMail(String m_strMail) {
        this.m_strMail = m_strMail;
    }

    public boolean validate(){
        return m_strName != null && !m_strName.isEmpty();
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.USER_INFO_FIELD_NAME_NAME, m_strName);
        values.put(DBConstants.USER_INFO_FIELD_NAME_SNAME, m_strSName);
        values.put(DBConstants.USER_INFO_FIELD_NAME_PHONE, m_strPhone);
        values.put(DBConstants.USER_INFO_FIELD_NAME_MAIL, m_strMail);
        return values;
    }
}
