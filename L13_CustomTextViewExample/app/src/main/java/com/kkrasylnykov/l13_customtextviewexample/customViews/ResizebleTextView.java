package com.kkrasylnykov.l13_customtextviewexample.customViews;

import android.content.Context;
import android.icu.util.Measure;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

public class ResizebleTextView extends TextView {
    public ResizebleTextView(Context context) {
        super(context);
    }

    public ResizebleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResizebleTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int nWidthMax = MeasureSpec.getSize(widthMeasureSpec);
        super.onMeasure(MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED), heightMeasureSpec);
        int nRealWidth = getMeasuredWidth();

        if (nWidthMax<nRealWidth){
            float fDelta = ((float)nWidthMax)/nRealWidth;
            float fTextSize = getTextSize() * fDelta;
            setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
            super.onMeasure(MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED), heightMeasureSpec);
            nRealWidth = getMeasuredWidth();
            while (nWidthMax<nRealWidth){
                fTextSize -= 5;
                setTextSize(TypedValue.COMPLEX_UNIT_PX, fTextSize);
                super.onMeasure(MeasureSpec.makeMeasureSpec(0,MeasureSpec.UNSPECIFIED), heightMeasureSpec);
                nRealWidth = getMeasuredWidth();
            }

        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
