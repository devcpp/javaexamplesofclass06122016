package com.kkrasylnykov.l26_threadsexample;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final static String KEY_PROGRESS = "KEY_PROGRESS";

    private TextView textView1, textView2;
    private Button button1, button2, button3, button4, button5, button6;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView1 = (TextView) findViewById(R.id.TextView1);
        textView2 = (TextView) findViewById(R.id.TextView2);

        button1 = (Button) findViewById(R.id.Button1);
        button2 = (Button) findViewById(R.id.Button2);
        button3 = (Button) findViewById(R.id.Button3);
        button4 = (Button) findViewById(R.id.Button4);
        button5 = (Button) findViewById(R.id.Button5);
        button6 = (Button) findViewById(R.id.Button6);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                int nProgress = msg.getData().getInt(KEY_PROGRESS);
                textView2.setText(nProgress + "%");
            }
        };
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                textView2.setText("2 sec...");
            }
        }, 2000);



    }
    int n = 0;

    Thread thread;

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Button1:
                textView1.setText("---TEXT---" + n++);
                break;
            case R.id.Button2:
                for (int i=0; i<5000000; i++){
                    int g = (i*150)/400;
                    textView2.setText("g -> " + g);
                }
                break;
            case R.id.Button4:
                if (thread!=null){
                    thread.interrupt();
                    Log.d("devcpp","Button4 -> " + thread.isInterrupted());
                }
                break;
            case R.id.Button3:
                thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int nOldProgress = -1;
                        long nSum = 0;
                        int nMax = 50000;
                        for (int i=0; i<nMax; i++){
                            int nProgress = (i*100)/nMax;
                            if (nOldProgress!=nProgress){
                                nOldProgress = nProgress;
                                Bundle bundle = new Bundle();
                                bundle.putInt(KEY_PROGRESS, nProgress);
                                Message message = new Message();
                                message.setData(bundle);
                                handler.sendMessage(message);
                            }
                            Log.d("devcpp","Button3 -> " + thread.isInterrupted());
                            if (thread.isInterrupted()){
                                final long nFinalSum = nSum;
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        textView2.setText("nSum -> " + nFinalSum);
                                    }
                                });
                                return;
                            }
                            int g = (int)Math.sqrt(((i*150)/400)/4);
                            nSum += g;
                            //Log.d("devcpp","nSum -> " + nSum);
                            try {
                                Thread.sleep(5);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                                thread.interrupt();
                                Log.e("devcpp","!!!!!!!!!!!!!!!!!!!! -> " + thread.isInterrupted());
                            }
                        }
                        final long nFinalSum = nSum;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textView2.setText("nSum -> " + nFinalSum);
                            }
                        });
                    }
                });
                thread.setDaemon(true);
                thread.start();
                break;
            case R.id.Button5:
                Thread threadAdd = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0;i<500000; i++){
                            addDollars();
                        }
                        Log.d("devcpp", "threadAdd -> " + getCashMoney());
                    }
                });

                Thread threadGet = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0;i<500000; i++){
                            getDollars();
                        }
                        Log.d("devcpp", "threadGet -> " + getCashMoney());
                    }
                });
                threadAdd.start();
                threadGet.start();
                break;
            case R.id.Button6:
                nEnd = 10000;
                for (int i=0; i<10000; i++){
                    final int n = i;
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Random random = new Random(System.currentTimeMillis());
                            final int nRandom =  random.nextInt(500);
                            int nRandomDelay = 1 + random.nextInt(9000);

                            Log.d("devcpp", n + " nRandom -> " + nRandom);
                            Log.d("devcpp", n + " nRandomDelay -> " + nRandomDelay);
                            try {
                                Thread.sleep(nRandomDelay);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isDone(nRandom);
                                }
                            });
                        }
                    });
                    thread.start();
                }
                break;
        }
    }

    int nSum = 0;
    int nEnd = 0;

    private void isDone(int nVal){
        nSum += nVal;
        nEnd -= 1;
        Log.d("devcpp","nEnd -> " + nEnd + " <<>> " + nSum);
        if (nEnd==0){
            textView1.setText("nSum -> " + nSum);
        }
    }

    private int nCashMoney = 10000;

    private Object o = new Object();

    public synchronized void addDollars(){
        nCashMoney += 10;
        /*synchronized (o){

        }*/
    }

    public synchronized void getDollars(){
        nCashMoney -= 10;
    }

    public int getCashMoney(){
        return nCashMoney;
    }
}
