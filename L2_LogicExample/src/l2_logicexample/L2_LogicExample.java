package l2_logicexample;

import java.util.Scanner;

public class L2_LogicExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Vvedite chislo:");
        int nA = scan.nextInt();
        
        long nResult;
        if ((nA>0) && (nA%2==0)){
            nResult = (nA*nA*nA);
        } else if (nA<0) {
            nResult = (nA*nA);
        } else {
            nResult = nA+10;
        }
        
        System.out.println("Result >> " + nResult);
    }
    
}
