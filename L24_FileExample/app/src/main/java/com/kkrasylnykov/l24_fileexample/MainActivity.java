package com.kkrasylnykov.l24_fileexample;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_PERMISSION_ON_WRITE_EXTERNAL_STORAGE = 1234;

    ArrayList<String> m_arrData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_ON_WRITE_EXTERNAL_STORAGE);
        } else {
            loadInfo();
        }
    }

    private void loadInfo(){
        m_arrData = getFileSystemInfo(Environment.getExternalStorageDirectory());
        String[] stringArray = m_arrData.toArray(new String[0]);
        ListView listView = (ListView) findViewById(R.id.ListViewMainActivity);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stringArray);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String strCurrent = m_arrData.get(i);
                startActivity(ViewerActivity.getStartIntent(MainActivity.this, strCurrent));
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode==REQUEST_PERMISSION_ON_WRITE_EXTERNAL_STORAGE && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
            loadInfo();
        } else {
            Toast.makeText(this, "NO PERMISSION!!!!", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private ArrayList<String> getFileSystemInfo(File file){
        ArrayList<String> arrResult = new ArrayList<>();
        if (file.isFile() && file.getName().contains(".jpg")){
            arrResult.add(file.getAbsolutePath());
        } else if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files!=null){
                for (File curFile:files){
                    arrResult.addAll(getFileSystemInfo(curFile));
                }
            }

        }
        return arrResult;
    }
}
