package com.kkrasylnykov.l24_fileexample;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

public class ViewerActivity extends AppCompatActivity {

    public static final String KEY_FILE_PATH = "KEY_FILE_PATH";

    public static Intent getStartIntent(Context context, String strFilePath){
        Intent intent = new Intent(context,ViewerActivity.class);
        intent.putExtra(KEY_FILE_PATH, strFilePath);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        String strFilePath = "";

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                strFilePath = bundle.getString(KEY_FILE_PATH, "");
            }
        }

        if (strFilePath.isEmpty()){
            showToastAndFinish("Error - File Path Is Empty!!!!");
        }

        File imgFile = new  File(strFilePath);
        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            ImageView myImage = (ImageView) findViewById(R.id.ImageViewViewerActivity);
            myImage.setImageBitmap(myBitmap);

        } else {
            showToastAndFinish("Error - File Isn't Exists!!!!");
        }
    }

    private void showToastAndFinish(String str){
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
        finish();
    }
}
