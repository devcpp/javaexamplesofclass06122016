package com.kkrasylnykov.l11_activityandlogsexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static  final int REQUEST_CODE_SECOND_ACTIVITY = 1001;

    private EditText m_EditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("devcpp","onCreate -> " + R.layout.activity_main);
        Button button = (Button) findViewById(R.id.btnMain);
        button.setOnClickListener(this);

        m_EditText = (EditText) findViewById(R.id.editTextMain);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart -> ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume -> ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","onRestart -> ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause -> ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop -> ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("devcpp","onDestroy -> ");
    }

    @Override
    public void onClick(View view) {
        Log.d("devcpp","onClick -> ");
        switch (view.getId()){
            case R.id.btnMain:
                String strName = m_EditText.getText().toString();
                Intent intent = new Intent(this, SecondActivity.class);
                intent.putExtra(SecondActivity.KEY_EXTRA_NAME, strName);
                //startActivity(intent);
                startActivityForResult(intent, REQUEST_CODE_SECOND_ACTIVITY);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("devcpp","onActivityResult -> requestCode -> " + requestCode +
                "; resultCode -> " + resultCode + "; data -> " + data);
        if (requestCode == REQUEST_CODE_SECOND_ACTIVITY){
            if (resultCode == RESULT_OK){
                if (data!=null && data.getExtras()!=null){
                    Log.d("devcpp","onActivityResult -> " + data.getExtras().getInt(SecondActivity.KEY_EXTRA_RETURN_ID_BUTTON, -101));
                }
            } else if (requestCode == RESULT_CANCELED) {

            }
        }
    }
}
