package com.kkrasylnykov.l11_activityandlogsexample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String KEY_EXTRA_NAME = "KEY_EXTRA_NAME";

    public static final String KEY_EXTRA_RETURN_ID_BUTTON = "KEY_EXTRA_RETURN_ID_BUTTON";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        String strName = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            strName = bundle.getString(KEY_EXTRA_NAME, "DevaultValue");
        }

        TextView textView = (TextView) findViewById(R.id.textViewSecond);
        textView.setText(strName);

        findViewById(R.id.positiveBtnSecond).setOnClickListener(this);
        findViewById(R.id.negativeBtnSecond).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.positiveBtnSecond:
                Intent intentOk = new Intent();
                intentOk.putExtra(KEY_EXTRA_RETURN_ID_BUTTON, R.id.positiveBtnSecond);
                setResult(RESULT_OK, intentOk);
                break;
            case R.id.negativeBtnSecond:
                setResult(RESULT_CANCELED);
                break;
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
