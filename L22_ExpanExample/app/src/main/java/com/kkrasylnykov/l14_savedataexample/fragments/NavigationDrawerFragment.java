package com.kkrasylnykov.l14_savedataexample.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

import java.util.zip.Inflater;

public class NavigationDrawerFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_navigation_drawer, container, true);

        final AppSettings settings = new AppSettings(getContext());
        CheckBox checkBox = (CheckBox)rootView.findViewById(R.id.isExitEditCheckBox);
        checkBox.setChecked(settings.isCloseActivity());

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                settings.setIsCloseActivity(b);
            }
        });
        return rootView;
    }
}
