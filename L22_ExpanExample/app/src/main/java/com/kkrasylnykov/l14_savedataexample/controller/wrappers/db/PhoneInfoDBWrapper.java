package com.kkrasylnykov.l14_savedataexample.controller.wrappers.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class PhoneInfoDBWrapper extends BaseDBWrapper {

    public PhoneInfoDBWrapper(Context context) {
        super(context, DBConstants.DB_V2.TABLE_PHONES.TABLE_NAME);
    }

    public ArrayList<PhoneInfo> getAll(){
        ArrayList<PhoneInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null);
        if (cursor!=null) {
            if (cursor.moveToFirst()) {
                do {
                    PhoneInfo phoneInfo = new PhoneInfo(cursor);
                    arrResult.add(phoneInfo);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }

    public PhoneInfo getPhoneById(long nId){
        PhoneInfo phoneInfo = null;
        SQLiteDatabase db = getReadableDB();

        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nId)};

        Cursor cursor = db.query(getTableName(),null,
                strSelect, arrArgs,null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                phoneInfo = new PhoneInfo(cursor);
            }
            cursor.close();
        }
        db.close();
        return phoneInfo;
    }

    public ArrayList<PhoneInfo> getPhoneByUserId(long nUserId){
        ArrayList<PhoneInfo> arrData = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();

        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nUserId)};

        Cursor cursor = db.query(getTableName(),null,
                strSelect, arrArgs,null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                do {
                    PhoneInfo phoneInfo = new PhoneInfo(cursor);
                    arrData.add(phoneInfo);
                } while (cursor.moveToNext());

            }
            cursor.close();
        }
        db.close();
        return arrData;
    }

    /*public ArrayList<PhoneInfo> getUserByField(String strSearch){
        String strSearchProcessing = strSearch + "%";
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        SQLiteDatabase db = getReadableDB();
        String strSelect = DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME + " LIKE ? OR " +
                DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME + " LIKE ? OR " +
                DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_PHONE + " LIKE ? OR " +
                DBConstants.DB_V1.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL + " LIKE ?";
        String[] arrArgs = new String[]{strSearchProcessing , strSearchProcessing, strSearchProcessing, strSearchProcessing};

        Cursor cursor = db.query(getTableName(),null,
                strSelect, arrArgs,null, null, null);
        if (cursor!=null) {
            if (cursor.moveToFirst()) {
                do {
                    UserInfo userInfo = new UserInfo(cursor);
                    arrResult.add(userInfo);
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        db.close();
        return arrResult;
    }*/

    public void update(PhoneInfo item){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        ContentValues values = item.getContentValues();
        db.update(getTableName(), values, strSelect, arrArgs);
        db.close();

    }

    public void insert(PhoneInfo item){
        SQLiteDatabase db = getWritableDB();

        ContentValues values = item.getContentValues();
        db.insert(getTableName(), null, values);
        db.close();

    }

    public void remove(PhoneInfo item){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(item.getId())};
        db.delete(getTableName(), strSelect, arrArgs);
        db.close();
    }

    public void removeById(long nId){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nId)};
        db.delete(getTableName(), strSelect, arrArgs);
        db.close();
    }

    public void removeByUserId(long nUserId){
        SQLiteDatabase db = getWritableDB();
        String strSelect = DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID + "=?";
        String[] arrArgs = new String[]{Long.toString(nUserId)};
        db.delete(getTableName(), strSelect, arrArgs);
        db.close();
    }

    public void removeAll(){
        SQLiteDatabase db = getWritableDB();

        db.delete(getTableName(),null,null);

        db.close();
    }
}
