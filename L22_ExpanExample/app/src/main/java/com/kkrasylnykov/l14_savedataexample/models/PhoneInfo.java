package com.kkrasylnykov.l14_savedataexample.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class PhoneInfo extends BaseInfo {
    private long m_nId = -1;
    private long m_nUserId = -1;
    private String m_strPhone;
    private boolean m_bIsNeedRemove = false;

    public PhoneInfo(long m_nUserId, String m_strPhone) {
        this.m_nId = -1;
        this.m_nUserId = m_nUserId;
        this.m_strPhone = m_strPhone;
        m_bIsNeedRemove = false;
    }

    public PhoneInfo(Cursor cursor) {
        this.m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_NAME_ID));
        this.m_nUserId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID));
        this.m_strPhone = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_PHONE));
        m_bIsNeedRemove = false;
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public long getUserId() {
        return m_nUserId;
    }

    public void setUserId(long m_nUserId) {
        this.m_nUserId = m_nUserId;
    }

    public String getPhone() {
        return m_strPhone;
    }

    public void setPhone(String m_strPhone) {
        this.m_strPhone = m_strPhone;
    }

    public boolean isNeedRemove() {
        return m_bIsNeedRemove;
    }

    public void setIsNeedRemove(boolean m_bIsNeedRemove) {
        this.m_bIsNeedRemove = m_bIsNeedRemove;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null && obj instanceof PhoneInfo){
            PhoneInfo o = (PhoneInfo) obj;
            bResult = o.getId()==this.m_nId;
        }
        return bResult;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_USER_ID, m_nUserId);
        values.put(DBConstants.DB_V2.TABLE_PHONES.USER_INFO_FIELD_PHONE, m_strPhone);
        return values;
    }
}
