package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;


public class DBConstants {

    public static final String DB_NAME = "db_phones";
    public static final int DB_VERSION = 6;

    public final static class DB_V1{
        public final static class TABLE_USER_INFO{
            public static final String TABLE_NAME = "UserInfo";

            public static final String USER_INFO_FIELD_NAME_ID = "_id";
            public static final String USER_INFO_FIELD_NAME_NAME = "_name";
            public static final String USER_INFO_FIELD_NAME_SNAME = "_sname";
            public static final String USER_INFO_FIELD_NAME_PHONE = "_phone";
            public static final String USER_INFO_FIELD_NAME_MAIL = "_mail";
        }
    }

    public final static class DB_V2{
        public final static class TABLE_USER_INFO{
            public static final String TABLE_NAME = "UserInfo";

            public static final String USER_INFO_FIELD_NAME_ID = "_id";
            public static final String USER_INFO_FIELD_NAME_NAME = "_name";
            public static final String USER_INFO_FIELD_NAME_SNAME = "_sname";
            public static final String USER_INFO_FIELD_NAME_MAIL = "_mail";
        }

        public final static class TABLE_PHONES{
            public static final String TABLE_NAME = "PhoneInfo";

            public static final String USER_INFO_FIELD_NAME_ID = "_id";
            public static final String USER_INFO_FIELD_USER_ID = "_user_id";
            public static final String USER_INFO_FIELD_PHONE = "_phone";
        }
    }



}
