package l3_arrayexample;

import java.util.Scanner;

public class L3_ArrayExample {

    public static void main(String[] args) {
        /*float fSum = 0;
        for(float x=-1.0f;x<5;x+=0.1f){
            float y = x*x;
            fSum+=y;
            System.out.println("y("+x+") = " + y);
        }
        System.out.println("fSum = " + fSum);*/
        
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Vvedite kol-vo storon:");
        int nCount = scanner.nextInt();
        
        float[] arrSizes = new float[nCount];
        
        for (int i=0; i<arrSizes.length; i++){
            System.out.println("Vvedite storonu " + (i+1));
            arrSizes[i]=scanner.nextFloat();
        }
        
        float fSum = 0;
        for (float fVal:arrSizes){
            fSum +=fVal;
        }
        
        System.out.println("fSum = " + fSum);
    }
    
}
