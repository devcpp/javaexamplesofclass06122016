package l4_setexample;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class L4_SetExample {
    
    public static void outSet(Set<Float> list){
        for (Float fVal:list){
            System.out.print(" " + fVal);
        }
        System.out.println();
    }
    
    public static void outArray(List<Float> list){
        for (Float fVal:list){
            System.out.print(" " + fVal);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        LinkedHashSet<Float> arrData = new LinkedHashSet<>();
        
        do{
            Float fSize = scanner.nextFloat();
            if (fSize>0){
                arrData.add(fSize);
            } else {
                break;//continue;
            }
        }while(true);
        
        outSet(arrData);
        
        boolean bRes = arrData.remove(3.2f);
        
        outSet(arrData);
        
        System.out.println("bRes -> " + bRes);
        System.out.println("contains -> " + arrData.contains(4f));
        
        ArrayList<Float> arrList = new ArrayList<>();
        arrList.addAll(arrData);
        
        
    }
    
}
