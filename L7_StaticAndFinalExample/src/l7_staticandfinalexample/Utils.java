package l7_staticandfinalexample;

public class Utils {
    
    public static final int MAX_COUNT = 456;
    
    public static int s_nCount = 0;
    private int m_nTime = 0;
    
    public Utils(){
        s_nCount++;
    }
    
    public static float cmToMm(float fVal){
        return fVal*10;
    }
    
    public static int abs(int a) {
        if (a<0) {
            a*=-1;
        }
        return a;
        //return (a < 0) ? -a : a;
    }
    
    public static int getCount(){
        return  s_nCount;
    }

    public static int getSum(Object... arrData){
        int nSum = 0;
        for(Object nVal:arrData){
            if (nVal instanceof Integer){
               nSum += (Integer) nVal;
            }
        }
        return nSum;
    }
}
