package com.kkrasylnykov.l26_gsonexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserInfo userInfo = new UserInfo();
        userInfo.setId(-1);
        userInfo.setName("Name");
        userInfo.setSName("SName");
        userInfo.setBDay(21);
        ArrayList<String> arrPhones = new ArrayList<>();
        arrPhones.add("Phone 1");
        arrPhones.add("Phone 2");
        arrPhones.add("Phone 3");
        userInfo.setPhones(arrPhones);

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        String strData = gson.toJson(userInfo);

        Log.d("devcpp", strData);

        String strInputData = "{\"phones\":[\"Phone 100\",\"Phone 200\",\"Phone 300\"],\"second_name\":\"SNameInput\",\"name\":\"NameInput\",\"bday\":340,\"id\":1076, \"test\":123}";

        UserInfo inputUserInfo = gson.fromJson(strInputData, UserInfo.class);

        Log.d("devcpp", inputUserInfo.toString());
    }
}
