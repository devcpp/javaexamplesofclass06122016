package com.kkrasylnykov.l28_servicesexample.toolsAndConstants;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.kkrasylnykov.l28_servicesexample.providers.AppSettingsContentProvider;

public class SettingApp {

    Context m_Context;

    public SettingApp(Context context){
        m_Context = context;
    }

    public int getClickCount(){
        Cursor cursor = m_Context.getContentResolver().query(AppSettingsContentProvider.COUNT_CLICK_URI, null, null, null, null);
        int nCountClick = 0;
        if (cursor!=null){
            if(cursor.moveToFirst()){
                nCountClick =  cursor.getInt(0);
            }
            cursor.close();
        }
        return nCountClick;
    }

    public int addClickCount(){
        int nClickCount = getClickCount() + 1;
        ContentValues values = new ContentValues();
        values.put(AppSettingsContentProvider.DATA, nClickCount);
        m_Context.getContentResolver().update(AppSettingsContentProvider.COUNT_CLICK_URI, values, null, null);
        return nClickCount;
    }
}
