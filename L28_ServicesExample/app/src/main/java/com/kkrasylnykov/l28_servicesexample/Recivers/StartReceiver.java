package com.kkrasylnykov.l28_servicesexample.recivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.kkrasylnykov.l28_servicesexample.services.StartService;

public class StartReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent startServiceIntent = new Intent(context, StartService.class);
        context.startService(startServiceIntent);
    }
}
