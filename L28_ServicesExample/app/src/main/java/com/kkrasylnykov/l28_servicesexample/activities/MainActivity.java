package com.kkrasylnykov.l28_servicesexample.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kkrasylnykov.l28_servicesexample.R;
import com.kkrasylnykov.l28_servicesexample.toolsAndConstants.SettingApp;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnNextActivity).setOnClickListener(this);
        findViewById(R.id.btnAddClickActivity).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnNextActivity:
                startActivity(DownloadActivity.getIntent(this, "http://p.xutpuk.pp.ua/test.zip"));
                break;
            case R.id.btnAddClickActivity:
                SettingApp settingApp = new SettingApp(MainActivity.this);
                int nOldCount = settingApp.getClickCount();
                int nCount = settingApp.addClickCount();
                Log.d("devcpp",nOldCount + " <> " + nCount);
                break;
        }
    }
}
