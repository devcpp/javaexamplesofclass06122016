package com.kkrasylnykov.l28_servicesexample.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.kkrasylnykov.l28_servicesexample.toolsAndConstants.SettingApp;

public class StartService extends Service {

    int n = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log();
        return super.onStartCommand(intent, flags, startId);
    }

    private void log(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SettingApp settingApp = new SettingApp(StartService.this);
                Log.d("devcpp","service -> ClickCount -> " + settingApp.getClickCount());
                log();
            }
        }, 2000);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
