package com.kkrasylnykov.l28_servicesexample.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

public class DownloadService extends Service {
    public static final String ACTION_DOWNLOAD = "com.kkrasylnykov.l28_servicesexample.services.DownloadService.ACTION_DOWNLOAD";
    public static final String ACTION_DOWNLOAD_KEY_TYPE = "TYPE";
    public static final String ACTION_DOWNLOAD_KEY_DATA = "DATA";
    public static final String ACTION_DOWNLOAD_KEY_TOTAL = "TOTAL";

    private static final String KEY_URL = "KEY_URL";

    public static Intent getIntent(Context context, String strUrl){
        Intent intent = new Intent(context, DownloadService.class);
        intent.putExtra(KEY_URL, strUrl);
        return intent;
    }

    private HashMap<String, Thread> hashMapData = new HashMap<>();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            String strUrl = "";
            if (bundle!=null){
                strUrl = bundle.getString(KEY_URL, "");
            }
            if(strUrl.isEmpty()){
                return super.onStartCommand(intent, flags, startId);
            }
            if (!hashMapData.containsKey(strUrl)){
                Thread thread = new Thread(new LoadFileRunnable(strUrl));
                hashMapData.put(strUrl, thread);
                thread.start();
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class LoadFileRunnable implements Runnable{
        private String m_strUrl;
        private String fileName;
        private long m_total = 0;

        public LoadFileRunnable(String strUrl){
            m_strUrl = strUrl;
        }

        @Override
        public void run() {
            try {
                URL url = new URL(m_strUrl);
                fileName = "test.zip";
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                m_total = connection.getContentLength();
                sendMessage("Length", m_total);

                InputStream input = connection.getInputStream();
                OutputStream output = new FileOutputStream(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + fileName);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    output.write(data, 0, count);
                    sendMessage("Progress", total);
                }

                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
                if (connection != null)
                    connection.disconnect();

                sendMessage("Done", 0);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void sendMessage(String strType, long data){
            Intent intent = new Intent();
            intent.setAction(ACTION_DOWNLOAD+m_strUrl);
            intent.putExtra(ACTION_DOWNLOAD_KEY_TYPE, strType);
            intent.putExtra(ACTION_DOWNLOAD_KEY_DATA, data);
            intent.putExtra(ACTION_DOWNLOAD_KEY_TOTAL, m_total);
            sendBroadcast(intent);
        }
    }
}
