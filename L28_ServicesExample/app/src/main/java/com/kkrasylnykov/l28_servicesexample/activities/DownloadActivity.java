package com.kkrasylnykov.l28_servicesexample.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kkrasylnykov.l28_servicesexample.R;
import com.kkrasylnykov.l28_servicesexample.services.DownloadService;

public class DownloadActivity extends AppCompatActivity {

    private static final String KEY_URL = "KEY_URL";

    ProgressBar progressBar;
    TextView nameTextView, progressTextView;
    long m_nTotal = 0;

    public static Intent getIntent(Context context, String strUrl){
        Intent intent = new Intent(context, DownloadActivity.class);
        intent.putExtra(KEY_URL, strUrl);
        return intent;
    }

    private String m_strUrl;
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent!=null){
                Bundle bundle = intent.getExtras();
                if (bundle!=null){
                    String strType =  bundle.getString(DownloadService.ACTION_DOWNLOAD_KEY_TYPE, "");
                    long nData =  bundle.getLong(DownloadService.ACTION_DOWNLOAD_KEY_DATA, 0);

                    switch (strType){
                        case "Length":
                            m_nTotal = nData;
                            break;
                        case "Progress":
                            m_nTotal = bundle.getLong(DownloadService.ACTION_DOWNLOAD_KEY_TOTAL, 0);
                            int curProgress = (int) (100 * (((float)nData)/m_nTotal));
                            if (progressTextView!=null){
                                progressTextView.setText("Download " + nData + " of " + m_nTotal);
                            }
                            if (progressBar!=null){
                                progressBar.setProgress(curProgress);
                            }
                            break;
                        case "Done":
                            if (progressTextView!=null){
                                progressTextView.setText("Done");
                            }
                            break;
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_strUrl = bundle.getString(KEY_URL, "");
            }
        }

        if (m_strUrl.isEmpty()){
            finish();
        }

        progressBar = (ProgressBar) findViewById(R.id.ProgressBar);
        nameTextView = (TextView) findViewById(R.id.textViewName);
        progressTextView = (TextView) findViewById(R.id.textViewProgress);

        nameTextView.setText(m_strUrl);

        startService(DownloadService.getIntent(this, m_strUrl));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(DownloadService.ACTION_DOWNLOAD+m_strUrl));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    public void pogress(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                int nProgress = progressBar.getProgress() + 1;
                progressBar.setProgress(nProgress);
                if (nProgress<progressBar.getMax()){
                    pogress();
                }
            }
        }, 300);
    }
}
