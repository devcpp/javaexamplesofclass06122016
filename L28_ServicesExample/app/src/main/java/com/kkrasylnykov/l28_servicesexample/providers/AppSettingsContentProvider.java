package com.kkrasylnykov.l28_servicesexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

public class AppSettingsContentProvider extends ContentProvider {
    private static final String AUTHORITY = "com.kkrasylnykov.l28_servicesexample.providers.ContentProvider";

    private static final String KEY_INTEGER_CLICK_COUNT = "KEY_INTEGER_CLICK_COUNT";

    public static final String DATA = "DATA";

    private SharedPreferences m_SharedPreferences;

    public static final String COUNT_CLICK      = "count_click";

    public static final Uri COUNT_CLICK_URI     = Uri.parse("content://" + AUTHORITY + "/" + COUNT_CLICK);

    public static final int URI_COUNT_CLICK = 1;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, COUNT_CLICK, URI_COUNT_CLICK);
    }

    @Override
    public boolean onCreate() {
        m_SharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        MatrixCursor result = new MatrixCursor(new String[]{DATA});
        switch (uriMatcher.match(uri)){
            case URI_COUNT_CLICK:
                int nClickCount = m_SharedPreferences.getInt(KEY_INTEGER_CLICK_COUNT, 0);
                Object[] data = new Object[]{nClickCount};
                result.addRow(data);
                break;
        }
        return result;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        switch (uriMatcher.match(uri)){
            case URI_COUNT_CLICK:
                int nClickCount = contentValues.getAsInteger(DATA);
                SharedPreferences.Editor editor = m_SharedPreferences.edit();
                editor.putInt(KEY_INTEGER_CLICK_COUNT, nClickCount);
                editor.commit();
                break;
        }
        return 0;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }
}
