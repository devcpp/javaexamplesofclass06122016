package com.kkrasylnykov.l14_savedataexample.controller.engines;

import android.content.Context;

import com.kkrasylnykov.l14_savedataexample.controller.wrappers.db.PhoneInfoDBWrapper;
import com.kkrasylnykov.l14_savedataexample.controller.wrappers.db.UserInfoDBWrapper;
import com.kkrasylnykov.l14_savedataexample.controller.wrappers.network.UserInfoNetworkWrapper;
import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import java.util.ArrayList;

public class UserInfoEngine extends BaseEngine {

    public UserInfoEngine(Context context) {
        super(context);
    }

    public ArrayList<UserInfo> getAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = dbWrapper.getAll();
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        for (UserInfo item:arrData){
            item.setPhones(phoneInfoDBWrapper.getPhoneByUserId(item.getId()));
        }
        if (arrData==null || arrData.size()==0){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(getContext());
            arrData = networkWrapper.getAll();
            for (UserInfo item:arrData){
                insert(item, false);
            }
        }
        return arrData;
    }

    public UserInfo getUserById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        UserInfo item = dbWrapper.getUserById(nId);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        item.setPhones(phoneInfoDBWrapper.getPhoneByUserId(nId));
        return item;
    }

    public ArrayList<UserInfo> getUserByField(String strSearch){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        ArrayList<UserInfo> arrData = dbWrapper.getUserByField(strSearch);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        for (UserInfo item:arrData){
            item.setPhones(phoneInfoDBWrapper.getPhoneByUserId(item.getId()));
        }
        return arrData;
    }

    public void update(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.update(item);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        for (PhoneInfo phone: item.getPhones()){
            if (phone.getId()>0){
                if (phone.getPhone().isEmpty() || phone.isNeedRemove()){
                    phoneInfoDBWrapper.remove(phone);
                } else {
                    phoneInfoDBWrapper.update(phone);
                }

            } else if (!phone.getPhone().isEmpty()) {
                phoneInfoDBWrapper.insert(phone);
            }
        }
        UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(getContext());
        networkWrapper.updateToServer(item);
    }
    public void insert(UserInfo item){
        insert(item, true);
    }

    public void insert(UserInfo item, boolean isSendToServer){
        if (isSendToServer){
            UserInfoNetworkWrapper networkWrapper = new UserInfoNetworkWrapper(getContext());
            networkWrapper.saveToServer(item);
        }
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        long nUserId = dbWrapper.insert(item);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        for (PhoneInfo phone: item.getPhones()){
            phone.setUserId(nUserId);
            phoneInfoDBWrapper.insert(phone);
        }
    }

    public void remove(UserInfo item){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.remove(item);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removeByUserId(item.getId());
    }

    public void removeById(long nId){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.removeById(nId);
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removeByUserId(nId);
    }

    public void removeAll(){
        UserInfoDBWrapper dbWrapper = new UserInfoDBWrapper(getContext());
        dbWrapper.removeAll();
        PhoneInfoDBWrapper phoneInfoDBWrapper = new PhoneInfoDBWrapper(getContext());
        phoneInfoDBWrapper.removeAll();
    }
}
