package com.kkrasylnykov.l14_savedataexample.controller.wrappers.network;


import android.content.Context;

public abstract class BaseNetworkWrapper {
    public final static int RESPONSE_CODE_OK = 200;

    private final static String BASE_URL = "http://xutpuk.pp.ua/";

    private Context m_Context;

    public BaseNetworkWrapper(Context context) {
        this.m_Context = context;
    }

    public Context getContext() {
        return m_Context;
    }

    public String getUrl(String strSubUrl){
        return BASE_URL + strSubUrl;
    }
}
