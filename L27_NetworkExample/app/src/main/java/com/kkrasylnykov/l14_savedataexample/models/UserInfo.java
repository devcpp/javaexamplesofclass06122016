package com.kkrasylnykov.l14_savedataexample.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserInfo extends BaseInfo {
    private long m_nId = -1;
    private long m_nServerId = -1;
    private String m_strName;
    private String m_strSName;
    private ArrayList<PhoneInfo> m_arrPhones;
    private String m_strMail;

    private boolean m_isExpand = false;

    public UserInfo(String m_strName, String m_strSName, ArrayList<PhoneInfo> m_arrPhones, String m_strMail) {
        this.m_strName = m_strName;
        this.m_strSName = m_strSName;
        this.m_arrPhones = m_arrPhones;
        this.m_strMail = m_strMail;
    }

    public UserInfo(Cursor cursor){
        m_nId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_ID));
        m_nServerId = cursor.getLong(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SERVER_ID));
        m_strName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME));
        m_strSName = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME));
        m_strMail = cursor.getString(cursor.getColumnIndex(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL));
    }

    public UserInfo(JSONObject jsonObject) throws JSONException {
        m_nId = -1;
        m_nServerId = jsonObject.getLong("id");
        m_strName = jsonObject.getString("name");
        m_strSName = jsonObject.getString("sname");
        JSONArray jsonArrayAddress = jsonObject.getJSONArray("address");
        if (jsonArrayAddress.length()>0){
            m_strMail = jsonArrayAddress.getString(0);
        } else {
            m_strMail = "";
        }
        JSONArray jsonArrayPhones = jsonObject.getJSONArray("phones");
        int nSizePhones = jsonArrayPhones.length();
        m_arrPhones = new ArrayList<>();
        for (int i = 0; i<nSizePhones; i++){
            m_arrPhones.add(new PhoneInfo(m_nId, jsonArrayPhones.getString(i)));
        }
    }

    public long getId() {
        return m_nId;
    }

    public void setId(long m_nId) {
        this.m_nId = m_nId;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getSName() {
        return m_strSName;
    }

    public void setSName(String m_strSName) {
        this.m_strSName = m_strSName;
    }

    public ArrayList<PhoneInfo> getPhones() {
        return m_arrPhones;
    }

    public void setPhones(ArrayList<PhoneInfo> m_arrPhones) {
        this.m_arrPhones = m_arrPhones;
    }

    public String getMail() {
        return m_strMail;
    }

    public void setMail(String m_strMail) {
        this.m_strMail = m_strMail;
    }

    public long getServerId() {
        return m_nServerId;
    }

    public void setServerId(long m_nServerId) {
        this.m_nServerId = m_nServerId;
    }

    public boolean validate(){
        return m_strName != null && !m_strName.isEmpty();
    }

    public boolean isExpand() {
        return m_isExpand;
    }

    public void setIsExpand(boolean m_isExpand) {
        this.m_isExpand = m_isExpand;
    }

    public ContentValues getContentValues(){
        ContentValues values = new ContentValues();
        values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SERVER_ID, m_nServerId);
        values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_NAME, m_strName);
        values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_SNAME, m_strSName);
        values.put(DBConstants.DB_V2.TABLE_USER_INFO.USER_INFO_FIELD_NAME_MAIL, m_strMail);
        return values;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject jsonBody = new JSONObject();
        if (getServerId()>=0){
            jsonBody.put("id", getServerId());
        }
        jsonBody.put("name", getName());
        jsonBody.put("sname", getSName());

        JSONArray jsonArrPhones = new JSONArray();
        ArrayList<PhoneInfo> arrPhones = getPhones();
        for(PhoneInfo phone:arrPhones){
            jsonArrPhones.put(phone.getPhone());
        }
        jsonBody.put("phones",jsonArrPhones);

        JSONArray jsonArrAddress = new JSONArray();
        jsonArrAddress.put(getMail());
        jsonBody.put("address",jsonArrAddress);

        return jsonBody;
    }

    @Override
    public boolean equals(Object obj) {
        boolean bResult = false;
        if (obj!=null && obj instanceof UserInfo){
            UserInfo o = (UserInfo) obj;
            bResult = o.getId()==this.m_nId;
        }
        return bResult;
    }
}
