package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.adapters.RecyclerAdapter;
import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.models.BaseInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_TERMS = 101;
    public static final int MOCK_USERS_COUNT = 30;
    public static final String NAME_SUFFIX = "_Name";
    public static final String SECOND_NAME_SUFFIX = "_SName";
    public static final String PHONE_PREFIX = "095444444";
    public static final String EMAIL_SUFFIX = "@test.com";

    List<BaseInfo> m_usersList;

    private String m_strSearch = "";
    private RecyclerAdapter adapter;

    private Toolbar m_toolbar;

    private DrawerLayout m_DrawerLayout = null;
    private View m_NavigationDrawer = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        

        m_DrawerLayout = (DrawerLayout) findViewById(R.id.DrawerLayoutMainActivity);
        m_NavigationDrawer = findViewById(R.id.NavigationDrawer);

        m_toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(m_toolbar);

        getSupportActionBar().setTitle("MAIN");

        Drawable menuIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_menu_black_24dp);
        menuIconDrawable.setColorFilter(ContextCompat.getColor(this, android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        m_toolbar.setNavigationIcon(menuIconDrawable);

        m_toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_DrawerLayout.openDrawer(m_NavigationDrawer);
            }
        });

        AppSettings settings = new AppSettings(this);
        if (!settings.isTermsAccept()) {
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_TERMS);
        }

        EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //TODO adapter.getFilter().filter(editable);
            }
        });

        findViewById(R.id.btnAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveAllUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnAddMockUsersMainActivity).setOnClickListener(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        m_usersList = new ArrayList<>();
        adapter = new RecyclerAdapter(this, m_usersList);
        adapter.setOnClickItem(new RecyclerAdapter.OnItemClickRecyclerAdapterListener() {
            @Override
            public void onItemLongClick(UserInfo item) {
                openAddActivity(item.getId());
            }
        });
        recyclerView.setAdapter(adapter);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

    private Thread updateThread;

    public void onUpdateList(){
        if (updateThread==null){
            updateThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    UserInfoEngine userInfoEngine = new UserInfoEngine(MainActivity.this);
                    m_usersList.clear();
                    m_usersList.addAll(userInfoEngine.getAll());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                        }
                    });
                    updateThread = null;
                }
            });
            updateThread.start();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        onUpdateList();
    }


    /*private void addMockUsers() {
        UserInfoEngine engine = new UserInfoEngine(this);
        for (int i = 0; i < MOCK_USERS_COUNT; i++) {
            String name = i + NAME_SUFFIX;
            String sName = i + SECOND_NAME_SUFFIX;
            String phone = PHONE_PREFIX + i;
            String email = name + EMAIL_SUFFIX;
            UserInfo userInfo = new UserInfo(name, sName, phone, email);
            engine.insert(userInfo);
        }
        adapter.updateList();
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnAddUserMainActivity:
                openAddActivity();
                break;
            case R.id.btnRemoveAllUserMainActivity:
                removeAllUsers();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TERMS && resultCode != RESULT_OK) {
            finish();
        }
    }

    private void removeAllUsers() {
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        userInfoEngine.removeAll();
        onUpdateList();
    }

    private void openAddActivity(long nId) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra(EditUserActivity.KEY_USER_ID, nId);
        startActivity(intent);
    }

    private void openAddActivity() {
        Intent intent = new Intent(this, EditUserActivity.class);
        startActivity(intent);
    }
}
