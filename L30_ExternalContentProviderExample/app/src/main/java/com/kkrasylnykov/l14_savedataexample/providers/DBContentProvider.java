package com.kkrasylnykov.l14_savedataexample.providers;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

public class DBContentProvider extends ContentProvider {
    private static final String AUTHORITY = "com.kkrasylnykov.l14_savedataexample.providers.DBContentProvider";

    private DBHelper m_DBHelper;

    public static final String TABLE_USER      = "user";
    public static final String TABLE_PHONE     = "phone";

    public static final Uri TABLE_USER_URI     = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER);
    public static final Uri TABLE_PHONE_URI     = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONE);

    public static final int URI_TABLE_USER = 1;
    public static final int URI_TABLE_PHONE = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, TABLE_USER, URI_TABLE_USER);
        uriMatcher.addURI(AUTHORITY, TABLE_PHONE, URI_TABLE_PHONE);
    }

    @Override
    public boolean onCreate() {
        m_DBHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] colums, String selection, String[] selectionArgs, String orderBy) {
        SQLiteDatabase db = getReadableDB();
        Cursor cursor = db.query(getType(uri), colums, selection, selectionArgs, null, null, orderBy);
        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        SQLiteDatabase db = getWritableDB();
        db.insert(getType(uri), null, contentValues);
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDB();
        return db.delete(getType(uri), selection, selectionArgs);
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        SQLiteDatabase db = getWritableDB();
        return db.update(getType(uri), contentValues,  selection, selectionArgs);
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        String strResult = "";
        switch (uriMatcher.match(uri)){
            case URI_TABLE_USER:
                strResult = DBConstants.DB_V2.TABLE_USER_INFO.TABLE_NAME;
                break;
            case URI_TABLE_PHONE:
                strResult = DBConstants.DB_V2.TABLE_PHONES.TABLE_NAME;
                break;
        }
        return strResult;
    }

    protected SQLiteDatabase getReadableDB(){
        return m_DBHelper.getReadableDatabase();
    }

    protected SQLiteDatabase getWritableDB(){
        return m_DBHelper.getWritableDatabase();
    }
}
