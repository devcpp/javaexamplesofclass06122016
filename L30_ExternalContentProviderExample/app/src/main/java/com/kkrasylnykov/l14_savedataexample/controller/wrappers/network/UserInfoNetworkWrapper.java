package com.kkrasylnykov.l14_savedataexample.controller.wrappers.network;

import android.content.Context;
import android.util.Log;

import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class UserInfoNetworkWrapper extends BaseNetworkWrapper {

    public UserInfoNetworkWrapper(Context context) {
        super(context);
    }

    public ArrayList<UserInfo> getAll(){
        ArrayList<UserInfo> arrResult = new ArrayList<>();
        String strURL = getUrl("api/users.json");
        URL url = null;
        try {
            url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");
            connection.setDoInput(true);
            connection.connect();

            int response = connection.getResponseCode();
            if (response==RESPONSE_CODE_OK){
                InputStream inputStream = connection.getInputStream();
                String strResponse = "";

                if (inputStream!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                JSONArray jsonArray = new JSONArray(strResponse);
                int nSize = jsonArray.length();
                for (int i = 0; i<nSize; i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    arrResult.add(new UserInfo(jsonObject));
                }
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrResult;
    }

    public void saveToServer(UserInfo item){
        String strURL = getUrl("api/users.json");
        try {
            String strBody =  item.toJSON().toString();
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("POST");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);
            connection.connect();

            int response = connection.getResponseCode();
            if (response==RESPONSE_CODE_OK){
                InputStream inputStream = connection.getInputStream();
                String strResponse = "";

                if (inputStream!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

                    if (reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr != null){
                                stringBuilder.append(tempStr);
                            } else {
                                strResponse = stringBuilder.toString();
                                break;
                            }
                        }
                    }
                }

                JSONObject jsonObject = new JSONObject(strResponse);
                item.setServerId(jsonObject.getLong("id"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateToServer(UserInfo item){
        String strURL = getUrl("api/users.json");
        try {
            String strBody =  item.toJSON().toString();
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty( "Content-Type", "application/json");
            connection.setRequestProperty( "charset", "utf-8");

            byte[] postData = strBody.getBytes(StandardCharsets.UTF_8);
            connection.getOutputStream().write(postData);
            connection.connect();

            int response = connection.getResponseCode();
            if (response==RESPONSE_CODE_OK){
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
