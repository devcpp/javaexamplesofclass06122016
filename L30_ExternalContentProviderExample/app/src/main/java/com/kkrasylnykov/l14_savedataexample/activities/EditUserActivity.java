package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String KEY_USER_ID = "KEY_USER_ID";


    private EditText m_nameEditText;
    private EditText m_snameEditText;
    //private EditText m_phoneEditText;
    private EditText m_mailEditText;

    private LinearLayout m_conteynerPhonesLinearLayout;
    private Button m_addPhoneButton;
    private LayoutInflater inflater;

    private long m_nUserId = -1;
    private long m_nServerId = -1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                m_nUserId = bundle.getLong(KEY_USER_ID, -1);
            }
        }

        inflater = LayoutInflater.from(this);

        m_nameEditText = (EditText) findViewById(R.id.editTextNameEditUserActivity);
        m_snameEditText = (EditText) findViewById(R.id.editTextSNameEditUserActivity);
        m_conteynerPhonesLinearLayout = (LinearLayout) findViewById(R.id.conteynerPhonesLinearLayoutEditUserActivity);
        m_addPhoneButton = (Button) findViewById(R.id.addPhoneButtonEditUserActivity);
        m_mailEditText = (EditText) findViewById(R.id.editTextMailEditUserActivity);

        m_addPhoneButton.setOnClickListener(this);

        Button btnAdd = (Button) findViewById(R.id.btnAddUserEditUserActivity);
        btnAdd.setOnClickListener(this);

        if (m_nUserId>=0){
            btnAdd.setText("Update");
            Button btnRemove = (Button) findViewById(R.id.btnRemoveUserEditUserActivity);
            btnRemove.setOnClickListener(this);
            btnRemove.setVisibility(View.VISIBLE);

            UserInfoEngine userInfoEngine = new UserInfoEngine(this);
            UserInfo userInfo = userInfoEngine.getUserById(m_nUserId);
            m_nServerId = userInfo.getServerId();
            m_nameEditText.setText(userInfo.getName());
            m_snameEditText.setText(userInfo.getSName());
            for (PhoneInfo item: userInfo.getPhones()){
                addPhoneFieldInConteiner(item);
            }
            addPhoneFieldInConteiner();
            m_mailEditText.setText(userInfo.getMail());
        } else {
            addPhoneFieldInConteiner();
        }

    }

    private void addPhoneFieldInConteiner(){
        addPhoneFieldInConteiner(null);
    }

    private void addPhoneFieldInConteiner(PhoneInfo phoneInfo){
        final LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.item_add_phone_layout, m_conteynerPhonesLinearLayout, false);
        m_conteynerPhonesLinearLayout.addView(linearLayout);
        if (phoneInfo!=null){
            EditText editText = (EditText) linearLayout.findViewById(R.id.phoneEditTextItem);
            editText.setText(phoneInfo.getPhone());
            linearLayout.setTag(phoneInfo.getId());
        }
        View btnRemovePhone = linearLayout.findViewById(R.id.removePhoneButtonItem);
        btnRemovePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (linearLayout.getTag()!=null){
                    linearLayout.setVisibility(View.GONE);
                } else {
                    m_conteynerPhonesLinearLayout.removeView(linearLayout);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnAddUserEditUserActivity:
                insertOrUpdateUserInfo();
                break;
            case R.id.btnRemoveUserEditUserActivity:
                UserInfoEngine userInfoEngine = new UserInfoEngine(this);
                userInfoEngine.removeById(m_nUserId);
                finish();
                break;
            case R.id.addPhoneButtonEditUserActivity:
                addPhoneFieldInConteiner();
                break;
        }
    }

    private void insertOrUpdateUserInfo(){
        ArrayList<PhoneInfo> arrPhones = new ArrayList<>();
        int nCount = m_conteynerPhonesLinearLayout.getChildCount();
        for (int i=0; i<nCount; i++){
            LinearLayout linearLayout = (LinearLayout) m_conteynerPhonesLinearLayout.getChildAt(i);
            EditText editText = (EditText) linearLayout.findViewById(R.id.phoneEditTextItem);
            String strPhone = editText.getText().toString();
            PhoneInfo phoneInfo = new PhoneInfo(m_nUserId, strPhone);
            phoneInfo.setUserId(m_nUserId);
            if (linearLayout.getTag()!=null){
                phoneInfo.setId((long)linearLayout.getTag());
                        /*Так делать не нужно!!!!!
                        if (linearLayout.getVisibility()==View.GONE){
                            phoneInfo.setIsNeedRemove(true);
                        } else {
                            phoneInfo.setIsNeedRemove(false);
                        }*/
                phoneInfo.setIsNeedRemove(linearLayout.getVisibility()==View.GONE);
            }
            arrPhones.add(phoneInfo);

        }
        final UserInfo userInfo = new UserInfo(m_nameEditText.getText().toString(),
                m_snameEditText.getText().toString(),
                arrPhones,
                m_mailEditText.getText().toString());
        userInfo.setId(m_nUserId);
        userInfo.setServerId(m_nServerId);
        final UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        if (m_nUserId>=0){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    userInfoEngine.update(userInfo);
                }
            }).start();
            finish();
        } else {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    userInfoEngine.insert(userInfo);

                    final AppSettings settings = new AppSettings(EditUserActivity.this);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (settings.isCloseActivity()){
                                finish();
                            } else {
                                m_nameEditText.setText("");
                                m_nameEditText.requestFocus();
                                m_snameEditText.setText("");
                                m_conteynerPhonesLinearLayout.removeAllViews();
                                addPhoneFieldInConteiner();
                                m_mailEditText.setText("");
                            }
                        }
                    });

                }
            }).start();
        }
    }
}
