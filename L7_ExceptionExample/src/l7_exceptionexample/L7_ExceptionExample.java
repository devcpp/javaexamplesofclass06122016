package l7_exceptionexample;

import java.util.InputMismatchException;
import java.util.Scanner;

public class L7_ExceptionExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try{
            int[] nMas = new int[]{1, 0, 5, 4, 8, 6};
        
            int nPos = scanner.nextInt();

            int fRes = 2/nMas[nPos];
            
            System.out.println(fRes);
            System.out.println(nMas[nPos]);

            float fData = scanner.nextFloat(); 
            return;
        } catch(ArrayIndexOutOfBoundsException e){
            //Код обработки ошибки
            System.out.println(e.toString());
        } catch (ArithmeticException e) {
            System.out.println(e.toString());
        } catch (InputMismatchException e) {
            System.out.println(e.toString());
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            System.out.println("finally!!!!!");
        }
        
        
        
    }
    
}
