package l5_oopexample;

public class Square {
    
    private int m_nSide;
    private int m_nArea;
    
    public Square(){
        System.out.println("Square()");
    }
    
    public Square(int nSide){
        System.out.println("Square(int nSide)");
        setSide(nSide);
    }
    
    public void setSide(int nSide){
        if (nSide>0){
            m_nSide = nSide;
        }        
    }
    
    public int getSide(){
        if (m_nSide>0){
            return m_nSide;
        }
        if (m_nArea>0){
            return (int)Math.pow(m_nArea, 0.5);
        }
        return 0;
    }
    
    public void setArea(int m_nArea){
        if (m_nArea>0){
            this.m_nArea = m_nArea;
        }        
    }
    
    public int getArea(){
        if (m_nArea>0){
            return m_nArea;
        }
        if (m_nSide>0){
            return m_nSide*m_nSide;
        }
        return 0;
    }
    
    public float getDiagonal(){
        float fResult = 0;
        fResult = (float)(getSide()*getSqrt());
        return fResult;
    }
    
    private float getSqrt(){
        return (float)Math.pow(2.0, 0.5);
    }   
}
