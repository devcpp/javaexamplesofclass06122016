package l5_oopexample;

import java.util.Scanner;

public class L5_OOPExample {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Square square = new Square();
        square.setSide(scan.nextInt());
        System.out.println("square -> " + square.getSide());
        System.out.println("square -> " + square.getDiagonal());
    }
    
}
