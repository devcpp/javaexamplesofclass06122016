package com.kkrasylnykov.l24_fileexample.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kkrasylnykov.l24_fileexample.R;
import com.kkrasylnykov.l24_fileexample.activities.ViewerActivity;

import java.io.File;

public class ImageFragment extends Fragment {

    public String m_strFilePath = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        File imgFile = new  File(m_strFilePath);
        if(imgFile.exists()){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            float fDeltaW = ((float)options.outWidth)/size.x;
            float fDeltaH = ((float)options.outHeight)/size.y;

            int nDelta = fDeltaW>fDeltaH ? (int)(fDeltaW+0.5):(int)(fDeltaH+0.5);

            nDelta = nDelta>0 ? nDelta : 1;

            options = new BitmapFactory.Options();
            options.inSampleSize = nDelta;


            /*Log.d("devcpp","file.w   -> " + options.outWidth);
            Log.d("devcpp","file.h   -> " + options.outHeight);
            Log.d("devcpp","fDeltaW  -> " + fDeltaW);
            Log.d("devcpp","fDeltaH  -> " + fDeltaH);
            Log.d("devcpp","nDelta   -> " + nDelta);*/


            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            Log.d("devcpp","myBitmap   -> " + myBitmap.getWidth());
            Log.d("devcpp","myBitmap   -> " + myBitmap.getHeight());
            Log.d("devcpp","screen.w -> " + size.x);
            Log.d("devcpp","screen.h -> " + size.y);
            ImageView myImage = (ImageView) view.findViewById(R.id.ImageViewImageFragment);
            myImage.setImageBitmap(myBitmap);

        } else {
            ((ViewerActivity)getActivity()).showToastAndFinish("Error - File Isn't Exists!!!!");
        }
        return view;
    }

    public void setFilePath(String m_strFilePath) {
        this.m_strFilePath = m_strFilePath;
    }
}
