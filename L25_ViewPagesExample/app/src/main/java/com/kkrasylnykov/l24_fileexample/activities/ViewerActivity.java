package com.kkrasylnykov.l24_fileexample.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.kkrasylnykov.l24_fileexample.R;
import com.kkrasylnykov.l24_fileexample.adapters.ViewPagerFragmentAdapter;

public class ViewerActivity extends AppCompatActivity {

    private static final float MAX_SCALE = 0.3f;

    public static final String KEY_FILES_PATH = "KEY_FILES_PATH";
    public static final String KEY_POSITION = "KEY_POSITION";

    public static Intent getStartIntent(Context context, String[] arrFilesPath, int nPosition){
        Intent intent = new Intent(context,ViewerActivity.class);
        intent.putExtra(KEY_FILES_PATH, arrFilesPath);
        intent.putExtra(KEY_POSITION, nPosition);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);

        String[] arrFilePath = null;
        int nPosition = -1;

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                arrFilePath = bundle.getStringArray(KEY_FILES_PATH);
                nPosition = bundle.getInt(KEY_POSITION, 0);
            }
        }

        if (arrFilePath==null){
            showToastAndFinish("Error - Files Paths Is Empty!!!!");
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.ViewPagerViewerActivity);
        ViewPagerFragmentAdapter adapter = new ViewPagerFragmentAdapter(getSupportFragmentManager(), arrFilePath);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(nPosition);
        viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                float scaleFactor = (1 - MAX_SCALE) * (1 - position) + MAX_SCALE;
                page.setScaleX(scaleFactor);
                page.setScaleY(scaleFactor);
            }
        });
    }

    public void showToastAndFinish(String str){
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
        finish();
    }
}
