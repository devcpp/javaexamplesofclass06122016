package com.kkrasylnykov.l24_fileexample.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kkrasylnykov.l24_fileexample.fragments.ImageFragment;

public class ViewPagerFragmentAdapter extends FragmentPagerAdapter {
    String[] m_arrData;

    public ViewPagerFragmentAdapter(FragmentManager fm, String[] arrData) {
        super(fm);
        m_arrData = arrData;
    }

    @Override
    public Fragment getItem(int position) {
        ImageFragment imageFragment = new ImageFragment();
        imageFragment.setFilePath(m_arrData[position]);
        return imageFragment;
    }

    @Override
    public int getCount() {
        return m_arrData.length;
    }
}
