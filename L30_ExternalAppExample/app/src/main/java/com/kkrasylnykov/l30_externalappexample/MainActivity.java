package com.kkrasylnykov.l30_externalappexample;

import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String AUTHORITY = "com.kkrasylnykov.l14_savedataexample.providers.DBContentProvider";

    public static final String TABLE_USER      = "user";
    public static final String TABLE_PHONE     = "phone";

    public static final Uri TABLE_USER_URI     = Uri.parse("content://" + AUTHORITY + "/" + TABLE_USER);
    public static final Uri TABLE_PHONE_URI     = Uri.parse("content://" + AUTHORITY + "/" + TABLE_PHONE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor = getContentResolver().query(TABLE_USER_URI, null, null, null, null);
        if (cursor!=null){
            if (cursor.moveToFirst()){
                int nColumnCount = cursor.getColumnCount();
                do{
                    for (int i=0; i< nColumnCount; i++){
                        Log.d("devcpp", "cursor.getColumnName -> " + cursor.getColumnName(i));
                        if (i==0 || i==1){
                            Log.d("devcpp", "data -> " + cursor.getLong(i));
                        } else {
                            Log.d("devcpp", "data -> " + cursor.getString(i));
                        }
                    }

                } while (cursor.moveToNext());
            }
            cursor.close();
        }
    }
}
