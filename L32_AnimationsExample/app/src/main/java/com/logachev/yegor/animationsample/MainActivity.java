package com.logachev.yegor.animationsample;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Slide;
import android.view.Gravity;
import android.view.View;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final int[] SRC_IMGS_RESOURCES = {R.drawable.girl_0, R.drawable.girl_1, R.drawable.girl_2,
            R.drawable.girl_3, R.drawable.girl_4, R.drawable.girl_5, R.drawable.girl_6, R.drawable.girl_7,
            R.drawable.girl_8, R.drawable.girl_9, R.drawable.girl_10, R.drawable.girl_11};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        findViewById(R.id.btnPropertyAnimations).setOnClickListener(this);
        findViewById(R.id.btnTransitionAnimations).setOnClickListener(this);
        findViewById(R.id.btnViewPagerAnimation).setOnClickListener(this);
        findViewById(R.id.btnListAnimation).setOnClickListener(this);
        findViewById(R.id.btnControllersTransitions).setOnClickListener(this);

        findViewById(R.id.error1).setOnClickListener(this);
        findViewById(R.id.error2).setOnClickListener(this);
        findViewById(R.id.error3).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnPropertyAnimations:
                onPropertyAnimationsClick();
                break;

            case R.id.btnTransitionAnimations:
                onTransitionAnimationsClick();
                break;

            case R.id.btnViewPagerAnimation:
                onViewPagerAnimationClick();
                break;

            case R.id.btnListAnimation:
                onListAnimationClick();
                break;

            case R.id.btnControllersTransitions:
                onControllersTransitionsClick();
                break;

            case R.id.error1:
                int n = 1/0;
                break;
            case R.id.error2:
                Activity activity = null;
                activity.getIntent();
                break;
            case R.id.error3:
                throw new RuntimeException("ERROR 3!!!!!!!!");
        }
    }

    private void startActivity(Class<? extends AppCompatActivity> cls) {
        startActivity(new Intent(this, cls));
    }

    private void onPropertyAnimationsClick() {
        startActivity(PropertyAnimActivity.class);
    }

    private void onTransitionAnimationsClick() {
        startActivity(TransitionAnimActivity.class);
    }

    private void onViewPagerAnimationClick() {
        startActivity(ViewPagerActivity.class);
    }

    private void onListAnimationClick() {
        startActivity(ViewAnimActivity.class);
    }

    private void onControllersTransitionsClick() {
        getWindow().setExitTransition(new Slide(Gravity.TOP));
        Bundle bundle = ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle();
        startActivity(new Intent(this, ScreenTransitionsActivity.class), bundle);
    }
}
