package com.kkrasylnykov.l9_listenerexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView m_inputTextView = null;
    private EditText m_outEditText = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_inputTextView = (TextView) findViewById(R.id.inputTextViewMainActivity);
        m_outEditText = (EditText) findViewById(R.id.outputEditTextMainActivity);

        Button positiveButton = (Button) findViewById(R.id.positiveButtonMainActivity);
        Button negativeButton = (Button) findViewById(R.id.negativeButtonMainActivity);

        positiveButton.setOnClickListener(this);
        negativeButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.positiveButtonMainActivity:
                String strText = m_outEditText.getText().toString();
                if (!strText.isEmpty()){
                    m_inputTextView.setText(strText);
                    m_outEditText.setText("");
                }
                break;
            case R.id.negativeButtonMainActivity:
                m_outEditText.setText("");
                break;
        }
    }
}
