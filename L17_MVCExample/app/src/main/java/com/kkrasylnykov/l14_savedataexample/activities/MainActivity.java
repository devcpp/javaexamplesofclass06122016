package com.kkrasylnykov.l14_savedataexample.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.db.DBHelper;
import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.AppSettings;
import com.kkrasylnykov.l14_savedataexample.toolsAndConstants.DBConstants;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_TERMS = 101;

    private LinearLayout m_container;

    private String m_strSearch = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppSettings settings = new AppSettings(this);
        if (!settings.isTermsAccept()){
            Intent intent = new Intent(this, TermsActivity.class);
            startActivityForResult(intent, REQUEST_TERMS);
        }

        EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                m_strSearch = charSequence.toString();
                updateScreen();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        findViewById(R.id.btnAddUserMainActivity).setOnClickListener(this);
        findViewById(R.id.btnRemoveAllUserMainActivity).setOnClickListener(this);
        m_container = (LinearLayout) findViewById(R.id.containerMAinActivity);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateScreen();
    }

    private void updateScreen(){
        m_container.removeAllViews();
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        ArrayList<UserInfo> arrData;
        if (m_strSearch.isEmpty()){
            arrData = userInfoEngine.getAll();
        } else {
            arrData = userInfoEngine.getUserByField(m_strSearch);
        }

        for (UserInfo userInfo : arrData){
            TextView text = new TextView(this);
            text.setText(userInfo.getName() + " " + userInfo.getSName() + "\nPhone: " + userInfo.getPhone());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,5,0,5);
            text.setLayoutParams(layoutParams);
            text.setOnClickListener(this);
            text.setTag(userInfo.getId());
            m_container.addView(text);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getTag()!=null){
            long nId = (long) view.getTag();
            openAddActivity(nId);
        } else {
            switch (view.getId()){
                case R.id.btnAddUserMainActivity:
                    openAddActivity();
                    break;
                case R.id.btnRemoveAllUserMainActivity:
                    removeAllUsers();
                    break;
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_TERMS && resultCode!=RESULT_OK){
            finish();
        }
    }

    private void removeAllUsers(){
        UserInfoEngine userInfoEngine = new UserInfoEngine(this);
        userInfoEngine.removeAll();

        updateScreen();
    }

    private void openAddActivity(long nId){
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra(EditUserActivity.KEY_USER_ID, nId);
        startActivity(intent);
    }

    private void openAddActivity(){
        Intent intent = new Intent(this, EditUserActivity.class);
        startActivity(intent);
    }
}
