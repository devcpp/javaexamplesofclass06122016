package com.kkrasylnykov.l14_savedataexample.toolsAndConstants;


public class DBConstants {

    public static final String DB_NAME = "db_phones";
    public static final int DB_VERSION = 1;

    public static final String TABLE_NAME_USER_INFO = "UserInfo";

    public static final String USER_INFO_FIELD_NAME_ID = "_id";
    public static final String USER_INFO_FIELD_NAME_NAME = "_name";
    public static final String USER_INFO_FIELD_NAME_SNAME = "_sname";
    public static final String USER_INFO_FIELD_NAME_PHONE = "_phone";
    public static final String USER_INFO_FIELD_NAME_MAIL = "_mail";
}
