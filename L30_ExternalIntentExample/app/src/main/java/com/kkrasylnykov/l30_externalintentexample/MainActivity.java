package com.kkrasylnykov.l30_externalintentexample;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.EditText);

        findViewById(R.id.emailButton).setOnClickListener(this);
        findViewById(R.id.internetButton).setOnClickListener(this);
        findViewById(R.id.phoneButton).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.internetButton:
                try{
                    Intent webIntent = new Intent();
                    webIntent.setAction(Intent.ACTION_VIEW);
                    webIntent.setData(Uri.parse(editText.getText().toString()));
                    startActivity(webIntent);
                } catch (ActivityNotFoundException e){
                    Toast.makeText(this, "ActivityNotFoundException", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.phoneButton:
                Intent intentCall = new Intent();
                //intentCall.setAction(Intent.ACTION_CALL);
                intentCall.setAction(Intent.ACTION_DIAL);
                intentCall.setData(Uri.parse("tel:" + editText.getText().toString()));
                startActivity(intentCall);
                break;
            case R.id.emailButton:
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"emailaddress@emailaddress.com"});
                intent.putExtra(Intent.EXTRA_CC, new String[]{"emailaddress_cc@emailaddress.com"});
                intent.putExtra(Intent.EXTRA_BCC, new String[]{"emailaddress_bcc@emailaddress.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, editText.getText().toString());
                intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

                startActivity(Intent.createChooser(intent, "Send Email"));
                break;
        }
    }
}
