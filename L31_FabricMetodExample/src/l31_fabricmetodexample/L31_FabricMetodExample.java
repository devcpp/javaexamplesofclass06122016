
package l31_fabricmetodexample;

import java.util.ArrayList;

public class L31_FabricMetodExample {

    public static void main(String[] args) {
        ArrayList<ConverToString> arrData = new ArrayList<>();
        arrData.add(new ModelUser());
        arrData.add(new ModelPhones());
        
        for(ConverToString o : arrData){
            System.out.println(o.converToString());
        }
    }
    
}
