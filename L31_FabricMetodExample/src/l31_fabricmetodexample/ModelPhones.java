package l31_fabricmetodexample;

import java.util.ArrayList;

public class ModelPhones extends ArrayList<Object> implements ConverToString{

    @Override
    public String converToString() {
        String strResult = "";
        for (Object o : this){
            strResult = o.toString() + "\n";
        }
        return strResult;
    }
    
}
