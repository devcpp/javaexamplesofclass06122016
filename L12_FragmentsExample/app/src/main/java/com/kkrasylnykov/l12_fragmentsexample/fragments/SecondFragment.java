package com.kkrasylnykov.l12_fragmentsexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kkrasylnykov.l12_fragmentsexample.R;

public class SecondFragment extends Fragment {

    private View m_rootView;

    private TextView m_TextView;
    private String m_strText = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        m_rootView = inflater.inflate(R.layout.fragment_second, container, false);
        m_TextView = (TextView) m_rootView.findViewById(R.id.textViewFragment2);
        if (!m_strText.isEmpty()){
            m_TextView.setText(m_strText);
        }
        return m_rootView;
    }

    public void setTextFragment(String strText){
        m_strText = strText;
        if (m_TextView!=null){
            m_TextView.setText(m_strText);
        }
    }
}
