package com.kkrasylnykov.l12_fragmentsexample.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kkrasylnykov.l12_fragmentsexample.R;
import com.kkrasylnykov.l12_fragmentsexample.activities.MainActivity;

public class FirstFragment extends Fragment implements View.OnClickListener {

    private View m_rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        m_rootView = inflater.inflate(R.layout.fragment_first, container, false);
        m_rootView.findViewById(R.id.btn1Fragment1).setOnClickListener(this);
        m_rootView.findViewById(R.id.btn2Fragment1).setOnClickListener(this);
        return m_rootView;
    }

    @Override
    public void onClick(View view) {
        String strText = "";
        switch (view.getId()){
            case R.id.btn1Fragment1:
                strText = "Вы нажали на 1 кнопку";
                break;
            case R.id.btn2Fragment1:
                strText = "Зря Вы нажали на 2 кнопку";
                break;
        }
        ((MainActivity)getActivity()).setText(strText);
    }
}
