package com.kkrasylnykov.l12_fragmentsexample.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.kkrasylnykov.l12_fragmentsexample.R;
import com.kkrasylnykov.l12_fragmentsexample.fragments.FirstFragment;
import com.kkrasylnykov.l12_fragmentsexample.fragments.SecondFragment;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private FirstFragment m_Fragment1;
    private SecondFragment m_Fragment2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        m_Fragment1 = new FirstFragment();
        m_Fragment2 = new SecondFragment();

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout1, m_Fragment1, "Fragment1");
        if (getResources().getConfiguration().orientation== Configuration.ORIENTATION_LANDSCAPE){
            transaction.add(R.id.frameLayout2, m_Fragment2, "Fragment2");
        }
        transaction.commit();

        /*findViewById(R.id.addFirstFragment).setOnClickListener(this);
        findViewById(R.id.removeFirstFragment).setOnClickListener(this);
        findViewById(R.id.addSecondFragment).setOnClickListener(this);
        findViewById(R.id.removeSecondFragment).setOnClickListener(this);*/
    }

    @Override
    public void onClick(View view) {
        /*FragmentTransaction transaction = getFragmentManager().beginTransaction();
        switch (view.getId()){
            case R.id.addFirstFragment:
                transaction.replace(R.id.frameLayout1, m_Fragment1, "Fragment1");
                break;
            case R.id.removeFirstFragment:
                transaction.remove(m_Fragment1);
                break;
            case R.id.addSecondFragment:
                Fragment fragment2 = getFragmentManager().findFragmentByTag("Fragment2");
                if (fragment2==null){
                    transaction.add(R.id.frameLayout1, m_Fragment2, "Fragment2");
                }
                break;
            case R.id.removeSecondFragment:
                transaction.remove(m_Fragment2);
                break;
        }
        transaction.commit();*/
    }

    public void setText(String strText){
        Log.d("devcpp", "setText -> " + strText);
        m_Fragment2.setTextFragment(strText);
        if (getResources().getConfiguration().orientation==Configuration.ORIENTATION_PORTRAIT){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.add(R.id.frameLayout1, m_Fragment2, "Fragment2");
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}
