package com.kkrasylnykov.l14_savedataexample.controller.engines;


import android.content.Context;

public abstract class BaseEngine {

    private Context m_Context;

    public BaseEngine(Context context){
        m_Context = context;
    }

    protected Context getContext(){
        return m_Context;
    }
}
