package com.kkrasylnykov.l14_savedataexample.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.kkrasylnykov.l14_savedataexample.R;
import com.kkrasylnykov.l14_savedataexample.controller.engines.UserInfoEngine;
import com.kkrasylnykov.l14_savedataexample.models.BaseInfo;
import com.kkrasylnykov.l14_savedataexample.models.PhoneInfo;
import com.kkrasylnykov.l14_savedataexample.models.UserInfo;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_ITEM = 1000;
    private static final int TYPE_ITEM_PHONE = 1001;
    private static final int TYPE_FIRST_TITLE = 1002;
    private static final int TYPE_SECOND_TITLE = 1003;

    private List<BaseInfo> usersList;
    private LayoutInflater inflater;

    private OnItemClickRecyclerAdapterListener listener;

    public RecyclerAdapter(Context context, List<BaseInfo> usersList) {
        inflater = LayoutInflater.from(context);
        this.usersList = usersList;
    }

    @Override
    public int getItemCount() {
        return usersList.size() + 2;
    }

    @Override
    public int getItemViewType(int position) {
        int nResult = TYPE_ITEM;
        if (position==0) {
            nResult = TYPE_FIRST_TITLE;
        } else if (position == getItemCount()-1) {
            nResult = TYPE_SECOND_TITLE;
        } else if (usersList.get(position-1) instanceof PhoneInfo) {
            nResult = TYPE_ITEM_PHONE;
        }
        return nResult;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolderResult = null;
        switch (viewType){
            case TYPE_ITEM:
                View itemView = inflater.inflate(R.layout.item_user_info, parent, false);
                viewHolderResult = new ItemViewHolder(itemView);
                break;
            case TYPE_ITEM_PHONE:
                View itemPhoneView = inflater.inflate(R.layout.item_phone_show, parent, false);
                viewHolderResult = new ItemPhoneViewHolder(itemPhoneView);
                break;
            case TYPE_FIRST_TITLE:
                View viewFirst = inflater.inflate(R.layout.item_first, parent, false);
                viewHolderResult = new FirstTitleViewHolder(viewFirst);
                break;
            case TYPE_SECOND_TITLE:
                View viewLast = inflater.inflate(R.layout.item_last, parent, false);
                viewHolderResult = new LastTitleViewHolder(viewLast);
                break;
        }
        return viewHolderResult;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){
            case TYPE_ITEM:
                ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
                final UserInfo userInfo = (UserInfo) usersList.get(position-1);
                itemViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int nPosition = usersList.indexOf(userInfo);
                        usersList.addAll(nPosition + 1, userInfo.getPhones());
                        for(int i=0; i<userInfo.getPhones().size(); i++){
                            RecyclerAdapter.this.notifyItemInserted(nPosition+i + 1);
                        }
                    }
                });
                itemViewHolder.nameTextView.setText(userInfo.getName() + " " + userInfo.getSName());
                itemViewHolder.nameBtn.findViewById(R.id.btnName).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(view.getContext(), userInfo.getName(), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case TYPE_ITEM_PHONE:
                ItemPhoneViewHolder itemPhoneViewHolder = (ItemPhoneViewHolder) holder;
                PhoneInfo phoneInfo = (PhoneInfo) usersList.get(position-1);
                itemPhoneViewHolder.phoneTextView.setText(phoneInfo.getPhone());
                break;
        }
    }

    public void setOnClickItem(OnItemClickRecyclerAdapterListener listener){
        this.listener = listener;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder{

        View rootView;
        TextView nameTextView;
        TextView phoneTextView;
        Button nameBtn;

        public ItemViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;

            nameTextView = (TextView) itemView.findViewById(R.id.textViewName);
            nameBtn = (Button) itemView.findViewById(R.id.btnName);
        }
    }

    public class ItemPhoneViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        TextView phoneTextView;

        public ItemPhoneViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            phoneTextView = (TextView) rootView.findViewById(R.id.showPhoneItem);
        }
    }

    public class FirstTitleViewHolder extends RecyclerView.ViewHolder{
        View rootView;

        public FirstTitleViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
        }
    }

    public class LastTitleViewHolder extends RecyclerView.ViewHolder{
        View rootView;

        public LastTitleViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
        }
    }

    public interface OnItemClickRecyclerAdapterListener {
        void onItemClick(UserInfo item);
    }
}
