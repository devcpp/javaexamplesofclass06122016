package l01_varexample;

import java.util.Scanner;

public class L01_VarExample {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int b = scanner.nextInt();
        int a = 16;
        int c = b+a;
        float f = (float)2.3;//2.3f;
        System.out.println("c = " + c);
    }
    
}
