package com.kkrasylnykov.l31_dialogsexample;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.Alert).setOnClickListener(this);
        findViewById(R.id.Wait).setOnClickListener(this);
        findViewById(R.id.Date).setOnClickListener(this);
        findViewById(R.id.Time).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Alert:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Test text");
                builder.setTitle("Warning!!!!");
                builder.setPositiveButton("Positive", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Positive Pressed!!!", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNegativeButton("Negative", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Negative Pressed!!!", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setNeutralButton("Neutral", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(MainActivity.this, "Neutral Pressed!!!", Toast.LENGTH_SHORT).show();
                    }
                });

                builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                });

                builder.setCancelable(false);

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
//                alertDialog.dismiss(); //Закрыть диалог
                //AlertDialog alertDialog = new AlertDialog();
                break;
            case R.id.Wait:
                final ProgressDialog progressDialog = new ProgressDialog(this);
                progressDialog.setMessage("Please Wait!");
                progressDialog.setCancelable(false);

                progressDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                }, 2000);
                break;
            case R.id.Date:
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Log.d("devcpp","y -> " + i + "; m -> " + i1 + "; d -> " + i2);
                    }
                };
                Calendar calendarDate = Calendar.getInstance();
                int curY = calendarDate.get(Calendar.YEAR);
                int curMONTH = calendarDate.get(Calendar.MONTH);
                int curD = calendarDate.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(this, onDateSetListener, curY, curMONTH, curD);
                datePickerDialog.show();
                break;
            case R.id.Time:
                TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Log.d("devcpp","h -> " + i + "; m -> " + i1);
                    }
                };
                Calendar calendar = Calendar.getInstance();
                int curH = calendar.get(Calendar.HOUR);
                int curM = calendar.get(Calendar.MINUTE);
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,onTimeSetListener, curH, curM, true);
                timePickerDialog.setMessage("Test");
                timePickerDialog.show();
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("devcpp","onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("devcpp","onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("devcpp","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("devcpp","onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("devcpp","onRestart");
    }
}
