package l3_sortexample;

public class L3_SortExample {

    public static void main(String[] args) {
        int[] arrData = {4,7,2,8,0,1};
        
        boolean bFlag = false;
        int j = 0;
        do{
            bFlag = false;
            j++;
            for(int i=0; i<(arrData.length-j); i++){
                if (arrData[i]>arrData[i+1]){
                    bFlag = true;
                    int tmp = arrData[i];
                    arrData[i] = arrData[i+1];
                    arrData[i+1] = tmp;
                }
            }
        }while(bFlag);
        
        for (int nVal:arrData){
            System.out.print(" " + nVal);
        }
        System.out.println();
    }
    
}
