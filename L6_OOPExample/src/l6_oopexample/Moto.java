package l6_oopexample;

import java.util.Scanner;

public class Moto extends Transport{
    
    private boolean m_bIsHaveSidecar;
    
    public void setHaveSidecar(boolean bIsHaveSidecar){
        m_bIsHaveSidecar = bIsHaveSidecar;
    }
    
    public boolean isHaveSidecar(){
        return m_bIsHaveSidecar;
    }

    @Override
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter Moto Model Name:");
        String strModelName = scanner.nextLine();
        setModelName(strModelName);
        
        System.out.println("Enter Moto Wheel Count:");
        int nCountWheel = scanner.nextInt();
        setWheelCount(nCountWheel);
        
        System.out.println("Enter Moto Power Engeen:");
        float fPowerEngeen = scanner.nextFloat();
        setPowerEngine(fPowerEngeen);
        
        scanner.nextLine();
        System.out.println("Enter Moto Color:");
        String strColor = scanner.nextLine();
        setColor(strColor);
        
        System.out.println("Moto Have Carside:");
        boolean bIsHaveSidecar = scanner.nextBoolean();
        setHaveSidecar(bIsHaveSidecar);
    }

    @Override
    public void outputData() {
        System.out.println("Moto " + getModelName());
        System.out.println("Wheel Count " + getWheelCount());
        System.out.println("Power Engine " + getPowerEngine());
        System.out.println("Color " + getColor());
        System.out.println("Have Sidecar " + isHaveSidecar());
    }

    @Override
    public boolean search(String strSearch) {
        return getModelName().contains(strSearch);
    }
    
}
