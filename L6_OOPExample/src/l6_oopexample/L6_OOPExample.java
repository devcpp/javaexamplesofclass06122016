package l6_oopexample;

import java.util.ArrayList;
import java.util.Scanner;

public class L6_OOPExample {
    
    private static ArrayList<Transport> m_arrData;

    public static void main(String[] args) {
        m_arrData = new ArrayList<>();
        
        Scanner scanner = new Scanner(System.in);
        
        int nType = -1;
        do{
            System.out.println("Menu:");
            System.out.println("1. Add");
            System.out.println("2. Show All");
            System.out.println("3. Search");
            System.out.println("4. Remove");
            System.out.println("5. Exit");
            nType = scanner.nextInt();
            
            switch(nType){
                case 1:
                    addItem();
                    break;
                case 2:
                    showAllItems();
                    break;
                case 3:
                    searchItems();
                    break;
                case 4:
                    removeAll();
                    break;
                /*default:
                    break;*/
            }
            
        } while (nType<5);
    }
    
    private static void addItem(){
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Menu:");
        System.out.println("1. Car");
        System.out.println("2. Moto");
        int nType = scanner.nextInt();
        
        Transport item = null;
        switch (nType){
            case 1:
                item = new Car();
                break;
            case 2:
                item = new Moto();
                break;
        }
        if (item!=null){
            item.inputData();
            m_arrData.add(item);
        } else {
            addItem();
        }
        
    }
    
    private static void showAllItems(){
        if (m_arrData!=null && m_arrData.size()>0){
            for (Transport item:m_arrData){
                item.outputData();
                System.out.println();
            }
        } else {
            System.out.println("Data not found!!!!!");
        }
    }
    
    private static void searchItems(){
        if (m_arrData!=null && m_arrData.size()>0){
            Scanner scanner = new Scanner(System.in);
            
            String strSearch = scanner.nextLine();
            for (Transport item:m_arrData){
                if (item.search(strSearch)){
                    item.outputData();
                    System.out.println();
                }
               
            }
        } else {
            System.out.println("Data not found!!!!!");
        }
    }
    
    private static void removeAll(){
        if (m_arrData!=null){
            m_arrData.clear();
        }
    }
    
}
