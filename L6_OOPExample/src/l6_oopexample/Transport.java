package l6_oopexample;

public abstract class Transport {
    
    private String m_strModelName;
    private int m_nWheelCount;
    private float m_fPowerEngine;
    private String m_strColor;
    
    public void setModelName(String strModelName){
        m_strModelName = strModelName;
    }
    
    public String getModelName(){
        return m_strModelName;
    }
    
    public void setWheelCount(int nWheelCount){
        m_nWheelCount = nWheelCount;
    }
    
    public int getWheelCount(){
        return m_nWheelCount;
    }
    
    public void setPowerEngine(float fPowerEngine){
        m_fPowerEngine = fPowerEngine;
    }
    
    public float getPowerEngine(){
        return m_fPowerEngine;
    }
    
    public void setColor(String strColor){
        m_strColor = strColor;
    }
    
    public String getColor(){
        return m_strColor;
    }
    
    public abstract void inputData();
    public abstract void outputData();
    public abstract boolean search(String strSearch);
}
