package l6_oopexample;

import java.util.Scanner;

public class Car extends Transport{
    
    private int m_nCountDoor;
    private String m_strCarcaseType;
    
    public void setCountDoor(int nCountDoor){
        m_nCountDoor = nCountDoor;
    }
    
    public int getCountDoor(){
        return m_nCountDoor;
    }
    
    public void setCarcaseType(String strCarcaseType){
        m_strCarcaseType = strCarcaseType;
    }
    
    public String getCarcaseType(){
        return m_strCarcaseType;
    }

    @Override
    public String getModelName() {
        return "Car " + super.getModelName();
    }

    @Override
    public void inputData() {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Enter Car Model Name:");
        String strModelName = scanner.nextLine();
        setModelName(strModelName);
        
        System.out.println("Enter Car Wheel Count:");
        int nCountWheel = scanner.nextInt();
        setWheelCount(nCountWheel);
        
        System.out.println("Enter Car Power Engeen:");
        float fPowerEngeen = scanner.nextFloat();
        setPowerEngine(fPowerEngeen);
        
        scanner.nextLine();
        System.out.println("Enter Car Color:");
        String strColor = scanner.nextLine();
        setColor(strColor);
        
        System.out.println("Enter Car Carcase Type:");
        String strCarcaseType = scanner.nextLine();
        setCarcaseType(strCarcaseType);
        
        System.out.println("Enter Car Door Count:");
        int nCountDoor = scanner.nextInt();
        setCountDoor(nCountDoor);
    }

    @Override
    public void outputData() {
        System.out.println(getModelName());
        System.out.println("Wheel Count " + getWheelCount());
        System.out.println("Power Engine " + getPowerEngine());
        System.out.println("Color " + getColor());
        System.out.println("Carcase Type " + getCarcaseType());
        System.out.println("Count Door " + getCountDoor());
    }

    @Override
    public boolean search(String strSearch) {
        return super.getModelName().contains(strSearch);
    }
    
}
